<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
	
);

return [
    'id' => 'app-backend',
	'name' => 'Lending',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
	'aliases' => [
		'@adminlte/widgets'=>'@vendor/adminlte/yii2-widgets',
		
	],
	// 'homeUrl' => '/admin',
    'modules' => [],
    'components' => [
        'request' => [
			'csrfParam' => '_csrf-backend',
			// 'class' => 'common\components\Request',
			// 'web'=> '/backend/web',
			// 'adminUrl' => '/admin'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
			'showScriptName' => false,
			'enablePrettyUrl' => true,
			'rules' => [
				// [
					// 'class' => 'yii\web\GroupUrlRule',
					// 'prefix' => 'admin',
				// ],
			],
        ],
		'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'lending505@gmail.com',
                'password' => 'lendingloan2021',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
		'urlManagerFrontend' => [

        	'class' => 'yii\web\urlManager',

        	'baseUrl' =>  __DIR__ ,

        	'enablePrettyUrl' => true,

        	'showScriptName' => false,

    	]
			
    ],
    'as access' => [
        'class' => yii\filters\AccessControl::class,
        'except' => ['site/error', 'site/login', 'site/logout'],
        'rules' => [
            ['allow' => true, 'roles' => ['@']],
        ],
    ],
    'params' => $params
];
