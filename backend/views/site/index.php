<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = 'Admin';

		
?>
<div class="site-index">
	
	<div class="row">
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
					<div class="col-md-12">
						<h3>Pending Registration</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $pendingCustomers,
								'columns' => [
									
									[
									  'attribute' => 'full_name',
									  'format' => 'raw', 
										'value' =>function($data){
											return $data->fullName;
										}
									],
									
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-pending-user}',
										
										'buttons'=> [
											'view-pending-user' => function ($url, $model) {     

												return Html::a('View', ['/customer/view', 'id' => $model->id],['target'=>'_blank']);                              

											}
											
										]
									]
								]
							]); ?>
							
							</br>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
				
					<div class="col-md-12">
						<h3>Pending Customer Loan</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $pendingLoans,
								'columns' => [
									
									[
									  'attribute' => 'full_name',
									  'format' => 'raw', 
										'value' =>function($data){
											return $data->customer->fullName;
										}
									],
									
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-pending-loan}',
										
										'buttons'=> [
											'view-pending-loan' => function ($url, $model) {     

												return Html::a('View', ['/loan/view', 'id' => $model->id],['target'=>'_blank']);                              

											}
											
										]
									]
								]
							]); ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
			
	</div>	
	
	<div class="row">
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
					<div class="col-md-12">
						<h3>Overdue Loan</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $overDueLoans,
								'columns' => [
									
									[
									  'attribute' => 'full_name',
									  'format' => 'raw', 
										'value' =>function($data){
											return $data->customer->fullName;
										}
									],
									
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-over-due-loan}',
										
										'buttons'=> [
											'view-over-due-loan' => function ($url, $model) {     

												return ($model->id != null)? Html::a('View', ['/loan/view', 'id' => $model->id],['target'=>'_blank']):"";                              

											}
											
										]
									]
								]
							]); ?>
							
						
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
			
	</div>	
</div>
