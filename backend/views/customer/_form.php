<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">
	
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3">
			<?= 
				$form->field($model, 'account_type')->dropDownList(
					array(""=>"Select Account Type")+array(
								1=>"Employee",
								2=>"Student",
								3=>"Visitor",
							),
						[
							'onchange' => '
								$.post( "'.Yii::$app->urlManager->createUrl('user/roles?id=').'"+$(this).val(), function( data ) {
									$( "#user-role_id" ).html( data );
								});
							',
						]
				);
			?>
		 </div>
		 <?php 
			if($model->role_id != $model->getPumRole()) {
		?>
			<div class="col-md-3">
				<?= $form->field($model, 'role_id')
					 ->dropDownList(
							($model->role != null)? array(""=>"Select Role")+ArrayHelper::map($model->listOfRoles, 'id', 'name'):array(""=>"Select Role")
						); 
				?>
				
			</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="col-md-3 school-form-display <?= ($model->account_type == $model::TYPE_VISITOR)? "hidden" :""?>">
			<?= $form->field($model, 'school_id')->textInput(['maxlength' => true]) ?>
		</div>
		
		
		<?php 
			if($model->status != $model::STATUS_PENDING  && $model->role_id != $model->getPumRole()) {
		?>
			<div class="col-md-3">
				<?= $form->field($model, 'status')
						 ->dropDownList(
								array(""=>"Select Status")+$model->getStatuses()
							)
				?>
				
			</div>
		<?php } ?>
		<div class="col-md-3">
			<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
		</div>
		

	</div>
    
	<div class="row">
		<div class="col-md-3">
			<?= $form->field($model, 'gender')->dropDownList(
							array(
							""=>"Select Gender",
							1=>"Male",
							2=>"Female"
							)
						)
			?>
		</div>
		<div class="col-md-3">

			<?= $form->field($model,'birthday')->widget(DatePicker::className(),['clientOptions' => ['dateFormat' => 'yyyy-mm-dd'],'options'=>['class'=>'form-control']]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'mobile_number')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
	
		<div class="col-md-12">
			<h4>Account Credential</h4>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'new_password')->passwordInput() ?>
		</div>
		
	</div>
	<div class="row">
	
		<div class="col-md-12">
			<h4>Address Details</h4>
		</div>
		<div class="col-md-3">
			<?= $form->field($addresses, 'street')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($addresses, 'barangay')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($addresses, 'city')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($addresses, 'province')->textInput(['maxlength' => true]) ?>
		</div>
		
	</div>

	<div class="row school-form-display <?= ($model->account_type == $model::TYPE_VISITOR)? "hidden" :""?>">
	
		<div class="col-md-12">
			<h4>School Details</h4>
		</div>
		<div class="col-md-3">
			<?= 
				$form->field($model, 'college_id')->dropDownList(
					array(""=>"Select College")+ArrayHelper::map($model->getCollegeLists(), 'id', 'name'),
					[
						'onchange' => '
							$.post( "'.Yii::$app->urlManager->createUrl('user/courses?id=').'"+$(this).val(), function( data ) {
								$( "#user-course_id" ).html( data );
							});
						',
					]
				);
			?>
		</div>
		<div class="col-md-3 course-form-display">
			<?= $form->field($model, 'course_id')
					 ->dropDownList(
							($model->course != null)? array(""=>"Select Course",$model->course->id => $model->course->name):array(""=>"Select Course")
						); //student only 
			?>
		</div>
		<div class="col-md-3 department-form-display">
			<?= $form->field($model, 'department_id')
					 ->dropDownList(
							array(""=>"Select Department")+ArrayHelper::map($model->getDepartmentLists(), 'id', 'name')
						) //employee only ?>
		</div>
	</div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
