<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


?>
	



<div class="user-view">
	
    <div class="container">
		<div class="row">
			<?php if($model->status == $model::STATUS_PENDING) { ?>
			<div class="col-md-12">
				<?= Html::a('Approve Registration', ['approve-registration','id'=>$model->id], ['class' => 'btn btn-success pull-right']) ?>
			</div>
			<?php } ?>
		</div>
		<div class="row">
			
			<div class="col-md-12">
				<div class="row panel panel-default">
					<div class="container-fluid">	
						<div class="row">
							<div class="col-md-12">
								<h3>Personal Details</h3>
							</div>
							<div class="col-md-6">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Basic Information</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>First Name</td>
											<td><?= $model->first_name ?></td>
										</tr>
										<tr>
											<td>Middle Name</td>
											<td><?= $model->middle_name ?></td>
										</tr>
										<tr>
											<td>Last Name</td>
											<td><?= $model->last_name ?></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><?= $model->email ?></td>
										</tr>
										
									</tbody>
								</table>
								
								</br>
							</div>
							<div class="col-md-6">
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Address Details</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>House number</td>
											<td><?= $model->address->house_number ?></td>
										</tr>
										<tr>
											<td>Street</td>
											<td><?= $model->address->street ?></td>
										</tr>
										<tr>
											<td>Barangay</td>
											<td><?= $model->address->barangay ?></td>
										</tr>
										<tr>
											<td>City</td>
											<td><?= $model->address->city ?></td>
										</tr>
										<tr>
											<td>Province</td>
											<td><?= $model->address->province ?></td>
										</tr>
										<tr>
											<td>Postal Code</td>
											<td><?= $model->address->post_code ?></td>
										</tr>
									</tbody>
								</table>
								
							</div>
							<div class="col-md-6">
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Employment Details</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>House number</td>
											<td><?= $model->address->house_number ?></td>
										</tr>
										<tr>
											<td>Street</td>
											<td><?= $model->address->street ?></td>
										</tr>
										<tr>
											<td>Barangay</td>
											<td><?= $model->address->barangay ?></td>
										</tr>
										<tr>
											<td>City</td>
											<td><?= $model->address->city ?></td>
										</tr>
										<tr>
											<td>Province</td>
											<td><?= $model->address->province ?></td>
										</tr>
										<tr>
											<td>Postal Code</td>
											<td><?= $model->address->post_code ?></td>
										</tr>
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		
			
				
		</div>	
	</div>	
	
	<div class="container">	
		<div class="row panel panel-default">
			<div class="container">	
				<h3>Documents</h3>
				<table class="table table-bordered">
					<thead>
						<th>File</th>
						<th>Action</th>
					</thead>
					<tbody>
						<?php 
							
							if($model->documents != null && $model->signature != null) {
								foreach($model->documents as $key => $value)
								{
									 
									$keyString = "";
									if($key == "valid_id") {
										$keyString="Valid ID";
									} else if($key == "secondary_id") {
										$keyString="Secondary ID";
									} else if($key == "proof_of_billing") {
										$keyString="Proof of Billing";
									}
									
									if($keyString != "") {
										echo "<tr>";
										
										echo "<td>".$keyString."</td>";
										echo "<td><a href=".Yii::getAlias('@web').'/uploads/customer/documents/'.$value." target='_blank'> View Docs</a></td>";
										echo "</tr>";
									}
									
									
								
								}
								echo "<tr>";
									echo "<td>Signature</td>";
									echo "<td><a href=".Yii::getAlias('@web').'/uploads/customer/signature/'.$model->signature." target='_blank'> View Docs</a></td>";
									echo "</tr>";
							} else {
								echo "<tr><td colspan='2'>No found record.</td></tr>";
							}
						?>
						
					</tbody>
				</table>

				</br>
			</div>

		</div>

	</div>
	
	<div class="container">	
		<div class="row panel panel-default">
			<div class="container">	
				<h3>Additional Documents</h3>
				<table class="table table-bordered">
					<thead>
						<th>File</th>
						<th>Action</th>
					</thead>
					<tbody>
						<?php 
							$loanChecker = false;
							if($model->loans != null) {
								foreach ($model->loans as $loans) { 
									
									foreach ($loans->loanAttachments as $attachment) { 
										$loanChecker = true;
									?>
										<tr>
											<td><?= $attachment->filename ?></td>
											<td><a href="<?= Yii::getAlias('@web').'/uploads/customer/attachments/'.$attachment->path ?>" target="_blank"> View Docs</a></td>
										</tr>
					<?php 			}
								}
								
								if($loanChecker == false) { ?>
									<td colspan="2">No found record.</td>
							<?php	}
							} else { ?>
							<tr>
								<td colspan="2">No found record.</td>
							</tr>
					<?php }
					?>
						
					</tbody>
				</table>

				</br>
			</div>

		</div>

	</div>
</div>
