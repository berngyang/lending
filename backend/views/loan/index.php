<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm; 

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admins';
$this->params['breadcrumbs'][] = $this->title;
$SUPER_ADMIN = 1;

		
?>
<div class="loan-index">
<div class="table-responsive">
<?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
			  'attribute' => 'full_name',
			  'format' => 'raw', 
				'value' =>function($data){
					return $data->customer->fullName;
				}
			],
            'email:email',
			[
				'label' => 'Amount',
				'attribute' =>'amount',
				'contentOptions' => ['class' => 'col-lg-1'],
				'format'=>['decimal',2]
			],
			[
				'label' => 'Term',
				'attribute' =>'term',
				'format' => 'raw',
				'value' =>function($data){
					$termLabel = [
						1 => '1 Month',
						3 => '3 Months',
						6 => '6 Months',
						9 => '9 Months',
						12 => '12 Months'
					];
					   
					   
					return $termLabel[$data->term];
				},
				'filter'=> Html::dropDownList('LoansSearch[term]',$termParams,$model->getTermLabel(),['class' => 'form-control'])
			],
            [
			   'attribute' => 'status',
			   'format' => 'raw',
			   'value' =>function($data){
				   $statusLabel = [
					   0 => '<span class="text-primary">Pending</span>',
					   1 => '<span class="text-success">Active</span>',
					   4 => '<span class="text-success">Done</span>',
					   5 => '<span class="text-danger">Over Due</span>',
					   6 => '<span class="text-danger">Cancelled</span>'
				   ];
				   
				   
				   return $statusLabel[$data->status];
			   },
			   'filter'=> Html::dropDownList('LoansSearch[status]',$statusParams,$model->getStatuses(),['class' => 'form-control'])
			],
			
            ['class' => 
				
				'yii\grid\ActionColumn', 
				'header'=>'Actions',
				'template' => '{view}{approve}{decline} ',
				
				'buttons'=> [
					'approve' => function ($url, $model) {     

						return $model->status == $model::STATUS_PENDING ? Html::a('<span class="glyphicon glyphicon-check"></span>',"#", [
							'class'=>'btn-approve-customer-loan',
							'data-id'=>$model->id,
							'data-toggle'=>"modal",
							'data-target'=>"#approve-customer-loan-modal",
							'title' => Yii::t('yii', 'approve')
							
						]): "";                                

					}
					
				]
			]
        ]
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
	<div class="modal fade" id="activate-user-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<form id="form-activate-user" >
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Activate Customer</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible activate-user-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> User has been activated.
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="User[id]" id="activate_user_id" />
						<input type="hidden" name="User[status]" value="<?php echo $model::STATUS_ACTIVE;?>" />
					
						<div class="col-md-10 col-md-offset-1">

							<div class="form-group">
								<label>Do you want to activate this Customer?</label>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-success activate-user-submit">Yes</button>
				</div>
			</div>
	  
		</form>
		</div>
	</div>
	
</div>
