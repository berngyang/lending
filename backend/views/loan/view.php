<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


?>
	



<div class="loan-view" id="loan-approval-page">
	
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row panel panel-default">
					<div class="container-fluid">	
						<div class="row">
							<div class="col-md-6">
								<h3>Borrower</h3>
							</div>
							<div class="col-md-6">
								<?php if($model->status == $model::STATUS_PENDING) { ?>
								<h3>
									<a href="javascript:void(0)" class="btn btn-success pull-right" data-toggle ="modal" data-target="#approval-loan-modal" >Approve Loan</a>
									<a href="javascript:void(0)" class="btn btn-danger pull-right" data-toggle ="modal" data-target="#decline-loan-modal" >Decline Loan</a>
								</h3>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Customer Details</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>Full Name</td>
											<td><?= $model->customer->fullName ?></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><?= $model->customer->email ?></td>
										</tr>
										<tr>
											<td>Phone Number</td>
											<td><?= $model->customer->phone_number ?></td>
										</tr>
										<tr>
											<td>Age</td>
											<td><?= $model->customer->age ?></td>
										</tr>
										<tr>
											<td>Gender</td>
											<td><?= $model->customer->genderText ?></td>
										</tr>
										
										<tr>
											<td>Action</td>
											<td><a href="/customer/view?id=<?=$model->customer->id?>" class="btn btn-sm btn-primary">View Full Details</a></td>
										</tr>
									</tbody>
								</table>
								
								</br>
							</div>
							<div class="col-md-6">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Loan Details</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>Amount</td>
											<td><?= $model->amount ?></td>
										</tr>
										<tr>
											<td>Status</td>
											<td><?= $model->statusLabel ?></td>
										</tr>
										<tr>
											<td>Amortization</td>
											<td><?= $model->amortization ?></td>
										</tr>
										<tr>
											<td>interest</td>
											<td><?= $model->interest ?></td>
										</tr>
										<tr>
											<td>Term</td>
											<td><?= $model->term ?></td>
										</tr>
										<?php if( $model->total_paid > 0) { ?>
											<tr>
												<td>Total Paid</td>
												<td><?= $model->total_paid ?></td>
											</tr>
										<?php } ?>
										<tr>
											<td>Total Due</td>
											<td><?= $model->total_due ?></td>
										</tr>
										<tr>
											<td>Due Date</td>
											<td><?= $model->due_date ?></td>
										</tr>
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<h3>Remarks</h3>
							</div>
							<div class="col-md-6">
								<?php if($model->status == $model::STATUS_PENDING) { ?>
								<h3>
									<a href="javascript:void(0)" class="btn btn-primary pull-right" data-toggle ="modal" data-target="#ask-loan-modal" >Ask more details</a>
								</h3>
								<?php } ?>
							</div>
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<td>Remarks</td>
										<td>Created</td>
									  </tr>
									</thead>
									
									<tbody>
										<?php 
												if($model->loanRemarks != null) {
													foreach ($model->loanRemarks as $remark) { ?>
														<tr>
															<td><?= $remark->remarks ?></td>
															<td><?= $remark->created ?></td>
														</tr>
										<?php 		}
												} else { ?>
												<tr>
													<td colspan="2">No found record.</td>
												</tr>
										<?php }
										?>
										
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<h3>Attachments</h3>
							</div>
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<td>File Name</td>
										<td>Action</td>
									  </tr>
									</thead>
									
									<tbody>
										<?php 
											if($model->loanAttachments != null) {
												foreach ($model->loanAttachments as $attachment) { ?>
													<tr>
														<td><?= $attachment->filename ?></td>
														<td><a href="<?= Yii::getAlias('@web').'/uploads/customer/attachments/'.$attachment->path ?>" target="_blank"> View Docs</a></td>
													</tr>
								<?php 			}
											} else { ?>
											<tr>
												<td colspan="2">No found record.</td>
											</tr>
									<?php }
									?>
										
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<h3>Payments</h3>
							</div>
							
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<td>Amount</td>
										<td>Payment Status</td>
										<td>Payment Date</td>
										<td>Payment Type</td>
										<td>Remarks</td>
										<td>Action</td>
									  </tr>
									</thead>
									
									<tbody>
										<?php 
												if($model->payments != null) {
													foreach ($model->payments as $payment) { ?>
														<tr>
															<td><?= $payment->amount ?></td>
															<td><?= $payment->paymentStatusText ?></td>
															<td><?= $payment->payment_date ?></td>
															<td><?= $payment->paymentTypeText ?></td>
															<td><?= $payment->remarks ?></td>
															<?php if( $payment->payment_type == $payment::PAYMENT_TYPE_REMITTANCE) { ?>
																<td>
																	<?php if($payment->payment_status == $payment::PAYMENT_STATUS_PENDING) { ?>
																			<a href="#" class="btn btn-sm btn-success verify-payment-btn" data-id="<?=$payment->id?>" data-toggle ="modal" data-target="#verify-payment-modal" >Verify Payment</a>
																	<?php } ?>
																	<a href="<?= Yii::getAlias('@web').'/uploads/customer/remittance/'.$payment->slip ?>" target="_blank" class="btn btn-sm btn-primary btn-verify-payment">View Docs</a>
																
																</td>
															<?php } else {?>
																<td></td>
															<?php } ?>
															
														</tr>
										<?php 		}
												} else { ?>
												<tr>
													<td colspan="6">No found record.</td>
												</tr>
										<?php }
										?>
										
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
			
		</div>	
	</div>	
	
	<div class="modal fade" id="approval-loan-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<form id="form-approval-loan" >
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Approval of Loan</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible approval-loan-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Loan has been approved.
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="Loans[id]" id="loan_id" value="<?php echo $model->id;?>" />
						<input type="hidden" name="Loans[status]" value="<?php echo $model::STATUS_ACTIVE;?>" />
					
						<div class="col-md-10 col-md-offset-1">

							<div class="form-group">
								<label>Do you want to approve this loan?</label>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-success approve-loan-submit">Yes</button>
				</div>
			</div>
	  
		</form>
		</div>
	</div>
	<div class="modal fade" id="decline-loan-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<form id="form-decline-loan" >
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Declining of Loan</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible decline-loan-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Loan has been declined.
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="Loans[id]" id="loan_id" value="<?php echo $model->id;?>" />
						<input type="hidden" name="Loans[status]" value="<?php echo $model::STATUS_DECLINED;?>" />
						
					
						<div class="col-md-12">

							<div class="form-group">
								<label>Do you want to decline this loan?</label>
							</div>
							
							
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12">

							
							<div class="form-group">
								<textarea name="Loans[remarks]"  rows="5" cols="70"/> </textarea>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-success decline-loan-submit">Yes</button>
				</div>
			</div>
	  
		</form>
		</div>
	</div>
	<div class="modal fade" id="ask-loan-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<form id="form-ask-loan" >
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Ask more details</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible ask-loan-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Inquiry has been raised.
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="Loans[id]" id="loan_id" value="<?php echo $model->id;?>" />
					
						<div class="col-md-12">

							<div class="form-group">
								<label>What do you need to ask?</label>
							</div>
							
							
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12">

							
							<div class="form-group">
								<textarea name="Loans[remarks]"  rows="5" cols="70" required="true"/> </textarea>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-success ask-loan-submit">Yes</button>
				</div>
			</div>
	  
		</form>
		</div>
	</div>
	
	<div class="modal fade" id="verify-payment-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		 <?php $form = ActiveForm::begin(['id'=>'form-verify-payment']); ?>
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Verify Payment</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible success-verify-payment-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> <span class="message-verify-payment">Payment has been verified.</span>
							</div>
							
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="Payments[id]" id="payment_id"  />
						<input type="hidden" name="Payments[payment_status]" id="payment_status"  />
					
						<div class="col-md-12">

							<div class="form-group">
								<label>Remarks</label>
							</div>
							
							
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12">

							
							<div class="form-group">
								<textarea name="Payments[remarks]"  rows="5" cols="70" required="true"/> </textarea>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-success verify-payment-submit">Verified</button>
				  <button type="button" class="btn btn-default decline-payment-submit">decline</button>
				</div>
			</div>
	  
		<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
