<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
const SUPER_ADMIN = 1;
$items =[];

	if(Yii::$app->user->identity->role == SUPER_ADMIN) {
		$items =[
			['label' => Yii::$app->user->identity->fullName, 'options' => ['class' => 'header']],
			['label' => 'Dashboard', 'icon' => 'fa fa-tachometer ', 'url' => ['/'],],
			['label' => 'Admins', 'icon' => 'fa fa-group', 'url' => ['/admin'],],
			['label' => 'Customers', 'icon' => 'fa fa-group', 'url' => ['/customer'],],
			['label' => 'Loan', 'icon' => 'fa fa-money', 'url' => ['/loan'],],
			// ['label' => 'Flush Data','icon' => 'fa fa-recycle','url' => ['#'],
				// 'template'=> '<a href="{url}" data-toggle="modal" data-target="#flus-old-data-modal">{icon}{label}</a>',
			// ],
			['label' => 'Profile', 'icon' => 'fa fa-cog', 'url' => ['/admin/view','id'=>Yii::$app->user->identity->id],],
			['label' => 'Log out','icon' => 'fa fa-sign-out','url' => ['/site/logout'],
				'template'=> '<a href="{url}" data-method="post">{icon}{label}</a>',
			],
		];
	} else {
		$items =[
			['label' => Yii::$app->user->identity->fullName, 'options' => ['class' => 'header']],
			['label' => 'Dashboard', 'icon' => 'fa fa-tachometer ', 'url' => ['/'],],
			['label' => 'Customers', 'icon' => 'fa fa-group', 'url' => ['/customer'],],
			['label' => 'Loan', 'icon' => 'fa fa-money', 'url' => ['/loan'],],
			['label' => 'Profile', 'icon' => 'fa fa-cog', 'url' => ['/admin/view','id'=>Yii::$app->user->identity->id],],
			['label' => 'Log out','icon' => 'fa fa-sign-out','url' => ['/site/logout'],
				'template'=> '<a href="{url}" data-method="post">{icon}{label}</a>',
			],
		];
	}



?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <?=
        Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => $items
                ]
        )
        ?>
        
    </section>
    <!-- /.sidebar -->
</aside>
