<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\AlertLte;
use common\widgets\Alert;
?>

	  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<?= Alert::widget() ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
			<?php if (isset($this->blocks['content-header'])) { ?>
				<h1><?= $this->blocks['content-header'] ?></h1>
			<?php } else { ?>
				<h1>
					<?php
					if ($this->title !== null) {
						echo \yii\helpers\Html::encode($this->title);
					} else {
						echo \yii\helpers\Inflector::camel2words(
							\yii\helpers\Inflector::id2camel($this->context->module->id)
						);
						echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
					} ?>
				</h1>
			<?php } ?>

			<?=
			Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
			<?= $content ?>
        </section><!-- /.content -->
	</div><!-- /.content-wrapper -->
	
	<div class="modal fade" id="flus-old-data-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<!--<form id="form-pui-pum-user" >-->
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Flushing data</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible flush-old-data-message hidden">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Data has been flushed.
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="User[id]" id="pui_pum_user_id" />
					
					
						<div class="col-md-10 col-md-offset-2">
							<h4>Are you sure you want to Flush old data?</h4>
						</div>
						
						
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-success flush-old-data-submit">Yes</button>
				</div>
			</div>
	  
		<!-- </form>-->
		</div>
	</div>	