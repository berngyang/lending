<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\widgets\Alert;


?>
<header class="main-header">
        <!-- Logo -->
        <a href="/site/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b><?=$title?></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?=$title?></b> Administrator</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          
			<?php 
				 // NavBar::begin();

					// $menuItems = [
						// [
						// 'label' => 'Log out', 
						// 'url' => ['/site/logout'],
						// 'linkOptions' => [
							// 'data-method' => 'post'
							// ]
						// ],
					// ];

					// echo Nav::widget([
						// 'options'=> ['class' => 'navbar-nav ml-auto'],
						// 'items' => $menuItems,
					// ]);
				
				// NavBar::end();
			?>
	
        </nav>
      </header>
