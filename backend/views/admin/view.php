<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Admin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


?>
	



<div class="user-view">
		<div class="row">
			<div class="col-md-12">
				<div class="row panel panel-default">
					<div class="container-fluid">	
						<div class="row">
							<div class="col-md-6">
								<h3>Personal Details</h3>
							</div
							><div class="col-md-6">
								<h3>
									<?php if(Yii::$app->user->identity->role == $model::SUPER_ADMIN || $model->id == Yii::$app->user->identity->id) {?>
										<?= Html::a('Update', ['update','id'=>$model->id], ['class' => 'btn btn-success pull-right']) ?>
										<?= Html::a('Change Password', ['change-password','id'=>$model->id], ['class' => 'btn btn-primary pull-right']) ?>
									<?php } ?>
								</h3>
							</div>
							<div class="col-md-8 col-md-offset-2">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Basic Information</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>Username</td>
											<td><?= $model->username ?></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td><?= $model->first_name ?></td>
										</tr>
										<tr>
											<td>Last Name</td>
											<td><?= $model->last_name ?></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><?= $model->email ?></td>
										</tr>
										<tr>
											<td>Role</td>
											<td><?= $model->roleLabel ?></td>
										</tr>
										<tr>
											<td>Gender</td>
											<td><?= $model->genderText ?></td>
										</tr>
										<tr>
											<td>Phone Number</td>
											<td><?= $model->phone_number ?></td>
										</tr>
										
									</tbody>
								</table>
								
								</br>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
		
			
				
		</div>	
	
</div>
