<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">
	
    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
	
		<div class="col-md-12">
			<h4>Account Credential</h4>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'new_password')->passwordInput() ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'confirm_new_password')->passwordInput() ?>
		</div>
	</div>
	
	

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
