<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">
	
    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3">
			<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
		</div>
		

	</div>
	<div class="row">
		<div class="col-md-3">
			<?= $form->field($model, 'role')->dropDownList(
				array(""=>"Select Role")+array(
							1=>"Super Admin",
							2=>"Admin"
						)
			);
			?>
			
		</div>
	</div>
	
    
	<div class="row">
		<div class="col-md-3">
			<?= $form->field($model, 'gender')->dropDownList(
							array(
							""=>"Select Gender",
							1=>"Male",
							2=>"Female"
							)
						)
			?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
