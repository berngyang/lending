<?php

namespace app\models;
use Yii;

class CustomerDocuments extends \yii\db\ActiveRecord
{
	

	public $imageFile;
    /**
     * {@inheritdoc}
     */
	
    public static function tableName()
    {
        return 'customer_documents';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
			[['valid_id','secondary_id','proof_of_billing'], 'safe'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, pdf']
        ];
    }
   
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valid_id' => 'Valid ID',
            'secondary_id' => 'Secondary ID',
            'proof_of_billing' => 'Proof Of Billing'
        ];
    }
	
	public function upload()
    {
        if ($this->validate()) {
			$filePath = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::getAlias('@frontend').'/web/uploads/customer/documents/' .$filePath );
			$copied = copy(Yii::getAlias('@frontend').'/web/uploads/customer/documents/' .$filePath  ,Yii::getAlias('@backend').'/web/uploads/customer/documents/' .$filePath );
			return $filePath;
        } else {
            return false;
        }
    }
}
