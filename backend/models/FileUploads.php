<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file_uploads".
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $created
 * @property string|null $updated
 */
class FileUploads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file_uploads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created', 'updated'], 'safe'],
            [['filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'File',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }
}
