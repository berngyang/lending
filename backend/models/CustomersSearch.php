<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Customers;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class CustomersSearch extends Customers
{
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	
	public $full_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status','employment_id','address_id'], 'integer'],
            [['username', 'full_name','first_name', 'middle_name', 'last_name', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'birthday', 'verification_token',  'gender', 'phone_number', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customers::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		

        $this->load($params);
		
        if (!$this->validate()) {
            return $dataProvider;
        }

		
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status
        ]);
		$query->andFilterWhere(['<>','status',self::STATUS_INACTIVE]);
        $query->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['or', ['like','first_name', $this->full_name],  ['like','middle_name', $this->full_name],  ['like','last_name', $this->full_name]]);

		
        return $dataProvider;
    }
}
