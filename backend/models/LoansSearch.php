<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Loans;


class LoansSearch extends Loans
{
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	const STATUS_DONE = 4;
	const STATUS_OVER_DUE = 5;
	const STATUS_CANCELLED = 6;
	
	public $full_name;
	public $email;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status','customer_id','is_due'], 'integer'],
            [['term', 'amount','email','full_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loans::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		

        $this->load($params);
		
        if (!$this->validate()) {
            return $dataProvider;
        }

		
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'customer_id' => $this->customer_id,
            'is_due' => $this->is_due,
            'term' => $this->term
        ]);
		$query->orderBy([
			'created' => SORT_DESC
		]);
		
        return $dataProvider;
    }
}
