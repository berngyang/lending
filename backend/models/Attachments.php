<?php

namespace app\models;

use Yii;
use common\models\Customers;
use app\models\Loans;
class Attachments extends \yii\db\ActiveRecord
{
   
    public static function tableName()
    {
        return 'attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created','status','customer_id'], 'safe'],
            ['path', 'required'],
            ['file_name', 'required'],
            ['loan_id', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'file_name' => 'File name',
            'loan_id' => 'Loan',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }
	public function getLoan()
    {
        return $this->hasOne(Loans::className(), ['id' => 'loan_id']);
    }
	
}
