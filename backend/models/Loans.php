<?php

namespace app\models;

use Yii;
use common\models\Customers;
use common\models\Payments;
use app\models\Remarks;
use app\models\Attachments;

use yii\data\ActiveDataProvider;

use MessageBird\Client;
use MessageBird\Objects\Message;

class Loans extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	const STATUS_DONE = 4;
	const STATUS_OVER_DUE = 5;
	const STATUS_CANCELLED = 6;
	const STATUS_DECLINED = 7;
	
	
	const TERM_1 = 1;
	const TERM_3 = 3;
	const TERM_6 = 6;
	const TERM_9 = 9;
	const TERM_12 = 12;
	
	public $remarks;
    public static function tableName()
    {
        return 'loans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created','status','is_due','due_date','remarks'], 'safe'],
            ['customer_id', 'required'],
            ['amount', 'required'],
            ['amortization', 'required'],
            ['interest', 'required'],
            ['term', 'required'],
            ['total_due', 'required'],
            ['total_paid', 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'amount' => 'Amount',
            'amortization' => 'Amortization',
            'interest' => 'Interest',
            'term' => 'Term',
            'total_due' => 'Total Due',
            'total_paid' => 'Total Paid',
            'due_date' => 'Due Date',
            'created' => 'Created',
        ];
    }
	
	public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }
	
	public function getLoanRemarks()
    {
        return $this->hasMany(Remarks::className(), ['loan_id' => 'id']);
    }
	
	public function getLoanAttachments()
    {
        return $this->hasMany(Attachments::className(), ['loan_id' => 'id']);
    }
	public function getpayments()
    {
        return $this->hasMany(Payments::className(), ['loan_id' => 'id'])->andOnCondition(['<>','status', self::STATUS_DELETED]);
    }
	public static function getStatuses()

    {

        return array(

            "" => 'Select Status',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_DONE => 'Done',
            self::STATUS_OVER_DUE => 'Over Due',
            self::STATUS_CANCELLED => 'Cancelled',
            self::STATUS_DECLINED => 'Declined',

            );

    }
	public static function getTermLabel()
    {
        return array(

            "" => 'Select Term',
            self::TERM_1 => '1 Month',
            self::TERM_3 => '3 Months',
            self::TERM_6 => '6 Months',
            self::TERM_9 => '9 Months',
            self::TERM_12 => '12 Months',

            );

    }
	
	public function getStatusLabel()
	{
		$statusLabel = [
			self::STATUS_PENDING => '<label class="badge badge-warning">Pending</label>',
			self::STATUS_ACTIVE => '<label class="badge badge-primary">Active</label>',
			self::STATUS_INACTIVE => '<label class="badge badge-danger">Inactive</label>',
			self::STATUS_DELETED => '<label class="badge badge-danger">Deleted</label>',
			self::STATUS_DONE => '<label class="badge badge-success">Done</label>',
			self::STATUS_OVER_DUE => '<label class="badge badge-danger">Over Due</label>',
			self::STATUS_CANCELLED => '<label class="badge badge-danger">Cancelled</label>',
			self::STATUS_DECLINED => '<label class="badge badge-danger">Declined</label>',
		];
		return $statusLabel[$this->status];

	}
	
	public function declineLoan()
	{
		$messageText = "Your loan has been declined please log in to your account to see the details.";
		if($this->remarks != null) {
			
			$remarks = new Remarks;
			$remarks->remarks = $this->remarks;
			$remarks->loan_id = $this->id;
			return $this->save(false) && $this->sendDeclineNotif() && $remarks->save(false)&& $this->sendSms($messageText,$this->customer->phone_number);
		}
		
		return $this->save(false) && $this->sendDeclineNotif() && $this->sendSms($messageText,$this->customer->phone_number);
	}
	public function sendDeclineNotif()
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'loan-declined-html', 'text' => 'loan-declined-text'],
                ['loan' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->customer->email)
            ->setSubject('Loan Status ' . Yii::$app->name)
            ->send();
    }
	public function askDetails()
	{
		$messageText = "Your loan has been validated and please log in to your account to see the additional details that we need.";
		$remarks = new Remarks;
		$remarks->remarks = $this->remarks;
		$remarks->loan_id = $this->id;
		return $this->save(false) && $remarks->save(false) && $this->sendAskDetailsNotif() && $this->sendSms($messageText,$this->customer->phone_number);
	}
	public function sendAskDetailsNotif()
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'ask-details-html', 'text' => 'ask-details-text'],
                ['loan' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->customer->email)
            ->setSubject('Additional Information ' . Yii::$app->name)
            ->send();
    }
	public function sendApproveLoanNotif()
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'loan-declined-html', 'text' => 'loan-declined-text'],
                ['loan' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->customer->email)
            ->setSubject('Loan Status ' . Yii::$app->name)
            ->send();
    }
	public function getAllPendingLoans()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_PENDING
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	
	public function getAllOverDueLoans()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_OVER_DUE
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	public function formatPhoneNumber($number)
	{
		if($number[0] == "0"){
			return str_replace("+6", "0", $number);;
		} else if($number[0] == "6") {
			return "+".$number;
		}
		
		return $number;
	}
	public function sendSms($messageText,$number)
	{
		$number = $this->formatPhoneNumber($number);
		$messagebird = new Client('GP74bHPuYXstJjeGiHl7bE5mV');//TEST
		// $messagebird = new Client('ESIFreO1NoJfTLQDwcniTr4Hg');//PROD
		$message = new Message;
		$message->originator = '+639187856241';
		// $message->recipients = [ $number]; //PROD when upgraded]
		$message->recipients = [ '+639187856241'];
		$message->body = $messageText;
		$response = $messagebird->messages->create($message);
		return true;
	}
}
