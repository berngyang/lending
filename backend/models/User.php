<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class User extends \yii\db\ActiveRecord 
{
	
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	
	const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_UPDATE_PASSWORD = 3;
	
	const SUPER_ADMIN = 1;
	const ADMIN = 2;
    
	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;
	
	public $confirm_password;
	public $new_password;
	public $confirm_new_password;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		
        return [
			['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'on'=>'insert', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 5, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'on'=>'insert', 'message' => 'This email address has already been taken.'],
			[['status'], 'safe'],
			['phone_number', 'string', 'min' => 11,'max' => 11],
			
			
            ['role', 'required'],
            ['first_name', 'required'],
            ['last_name', 'required'],
            ['gender', 'required'],
			
            ['password', 'required'],
            ['password_hash', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['new_password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
			['new_password', 'required'],
			['confirm_password', 'required'],
			['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
			['confirm_password', 'required'],
			['confirm_new_password', 'compare', 'compareAttribute'=>'new_password', 'message'=>"Passwords don't match" ]
			
        ];
    }
	
	public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = [
			'first_name', 
			'last_name',
			'email',
			'gender',
			'role',
			'username',
			'password',
			'password_hash',
			'confirm_password',
		];
		$scenarios[self::SCENARIO_UPDATE] = [
			'first_name', 
			'last_name',
			'email',
			'gender',
			'role'
		];
		$scenarios[self::SCENARIO_UPDATE_PASSWORD] = [
			'password_hash',
			'password',
			'new_password',
			'confirm_new_password',
		];
        return $scenarios;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'new_password' => 'New Password',
            'confirm_password' => 'Confirm Password',
            'verification_token' => 'Verification Token',
            'role' => 'Role',
            'phone_number' => 'Phone Number',
            'gender' => 'Gender',
            'updated_at' => 'Updated',
            'created_at' => 'Created',
        ];
    }

	 
	public function userUpdate($address)
	{
		
		if($this->new_password != "") {
			$this->setPassword($this->new_password);
		}

		if ($this->validate() == false && $address->validate() == false) {

            return null;
        }
		
        return $this->save() && $address->save();
	}
	public static function getStatuses()
    {

        return array(

            "" => 'Select Status',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted',

            );

    }
	
	public function statusLabel() 
	{
		return [
		   self::STATUS_PENDING => '<span class="badge badge-primary">Pending</span>',
		   self::STATUS_ACTIVE => '<span class="badge badge-success">Active</span>',
		   self::STATUS_INACTIVE => '<span class="badge badge-warning">Inactive</span>',
		   self::STATUS_DELETED => '<span class="badge badge-danger">Deleted</span>',
		];
	}
	public function genderLabel() 
	{
		return [
			"" => "Unknown",
			1 => 'Male',
			2 => 'Female'
		];
	}
	public function getGenderText() 
	{
		$genderLists =[
			self::GENDER_MALE=> 'Male',
			self::GENDER_FEMALE=> 'Female',
		];
		
		return $genderLists[$this->gender];
	}
	public function getAllPendingUsers()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_PENDING,
        ]);
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}

	public function getFullName()
	{

		return $this->first_name.' '.$this->last_name;

	}
	public function getRoleLabel()
	{

		return ($this->role == self::SUPER_ADMIN)? "Super Admin": "Admin";

	}
	/**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
	
	
	
	public static function getRoleLists()
    {

        return array(

            "" => 'Select Role',
            self::SUPER_ADMIN => 'Super Admin',
            self::ADMIN => 'Admin',

            );

    }
	
	public function changePassword()
	{
		$this->password = $this->new_password;
		$this->setPassword($this->new_password);
		
		return $this->save();
	}
	
}
