<?php

namespace backend\controllers;

use Yii;
use app\models\User;
use backend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AdminController extends Controller
{
	const SUPER_ADMIN = 1;
	const ADMIN = 2;
	
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
		$model = new User();

		$searchModel = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 15];
		$statusParams = "";
		$courseParams = "";
		$departmentParams = "";
		$collegeParams = "";
		$roleParams = "";
		
		if(isset(Yii::$app->request->queryParams['UserSearch']['status'])) {
			$statusParams = Yii::$app->request->queryParams['UserSearch']['status'];
		}
		if(isset(Yii::$app->request->queryParams['UserSearch']['role'])) {
			$roleParams = Yii::$app->request->queryParams['UserSearch']['role'];
		}
		
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'statusParams' => $statusParams,
			'roleParams' => $roleParams,
			'model' => $model,
		]);
		
    }

    public function actionView($id)
    {
		
		if(Yii::$app->user->identity->role == self::ADMIN && Yii::$app->user->identity->id != $id) {
			$id =Yii::$app->user->identity->id;
		}
		
		$model =  $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }


    public function actionUpdate($id)
    {
		
		if(Yii::$app->user->identity->role == self::ADMIN && Yii::$app->user->identity->id != $id) {
			$id =Yii::$app->user->identity->id;
		}
        $model = $this->findModel($id);
		$model->scenario = User::SCENARIO_UPDATE;
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success',  "Admin has been updated!");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }
	public function actionChangePassword($id)
    {
		
		if(Yii::$app->user->identity->role == self::ADMIN && Yii::$app->user->identity->id != $id) {
			$id =Yii::$app->user->identity->id;
		}
        $model = $this->findModel($id);
		$model->scenario = User::SCENARIO_UPDATE_PASSWORD;
		
        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
			Yii::$app->session->setFlash('success',  "Password has been changed.!");
            return $this->redirect(['view', 'id' => $model->id]);
        }
		
		if($model->errors != null) {
			foreach($model->errors as $error){
				Yii::$app->session->setFlash('error',  $error);
			}
		}

        return $this->render('change-password', [
            'model' => $model
        ]);
    }
	
	public function actionActivate($id)
    {
		
		if(Yii::$app->user->identity->role == self::ADMIN && Yii::$app->user->identity->id != $id) {
			$id =Yii::$app->user->identity->id;
		}
		
        $model = $this->findModel($id);
	
		$model->status = self::STATUS_ACTIVE;
        if ($model->save(false)) {
			echo json_encode([
				'response'=>true,
				'message'=>"Success"
				
			]);
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}

       
    }
	
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	public function actionCreate()
    {
        $model = new User();
		$model->scenario = User::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post())) {
			
			$model->setPassword($model->password);
			$model->generateAuthKey();
			if($model->save()) {
				
				Yii::$app->session->setFlash('success',  "Admin has been created!");
				return $this->redirect(['view', 'id' => $model->id]);
			}
			
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
	
	public function actionFlushOldData()
    {		
		$date = date("Y-m-d", strtotime("-6 months"));
		$qrCodes = QrCodes::find()->where(['<','created',$date])->all();		
		$pumRelatedDocs = PumRelatedDocs::find()->where(['<','created',$date])->all();		
		$fileUploads = FileUploads::find()->where(['<','created',$date])->all();		
		TemperaturesCollections::deleteAll(['<','created',$date]);		
		Temperatures::deleteAll(['<','created',$date]);		
		// Scanned::deleteAll(['<','created',$date]);		
		PumFindings::deleteAll(['<','created',$date]);		
		Pum::deleteAll(['<','created',$date]);		
		CloseContacts::deleteAll(['<','created',$date]);		
		CloseContactsCollections::deleteAll(['<','created',$date]);		
		Checklists::deleteAll(['<','created',$date]);		
		ChecklistCollections::deleteAll(['<','created',$date]);		
		
		foreach($qrCodes as $qrCode){
			$path = Yii::getAlias('@frontend/web/qrcodes/'.$qrCode->uuid.".png");
			if (file_exists($path) && $qrCode->uuid != "") {
				unlink($path);
			}
			$qrCode->delete();
		}
		foreach($pumRelatedDocs as $pumRelatedDoc){
			$path = Yii::getAlias('@backend/web/uploads/pums/'.$pumRelatedDoc->filename);
			if (file_exists($path) && $pumRelatedDoc->filename != "") {
				unlink($path);
			}
			$pumRelatedDoc->delete();
		}
		
		foreach($fileUploads as $fileUpload){
			$path = Yii::getAlias('@backend/web/uploads/checklists/'.$fileUpload->filename);
			if (file_exists($path) && $fileUpload->filename != "") {
				unlink($path);
			}
			$fileUpload->delete();
		}
		
		
		
        return true;
		
    }
}
