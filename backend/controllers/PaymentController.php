<?php

namespace backend\controllers;

use Yii;
use common\models\Payments;
use backend\models\AddressesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class PaymentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actionVerify($id)
	{
		$postData = Yii::$app->request->post();
		$model =  $this->findModel($id);
		
		if ($model->load($postData) && $model->verifyPayment()) {
			
			if($postData['Payments']['payment_status'] == $model::PAYMENT_STATUS_PAID) {
				echo json_encode([
					'response'=>true,
					'message'=>"Payment has been verified."
					
				]);
			} else {
				echo json_encode([
					'response'=>true,
					'message'=>"Payment has been declined."
					
				]);
			}
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}
	}
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
