<?php

namespace backend\controllers;

use Yii;
use app\models\Loans;
use app\models\LoansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use MessageBird\Client;
use MessageBird\Objects\Message;
class LoanController extends Controller
{
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
		$model = new Loans();

		$searchModel = new LoansSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 15];
		$statusParams = "";
		$termParams = "";
		
		if(isset(Yii::$app->request->queryParams['LoansSearch']['status'])) {
			$statusParams = Yii::$app->request->queryParams['LoansSearch']['status'];
		}
		if(isset(Yii::$app->request->queryParams['LoansSearch']['term'])) {
			$termParams = Yii::$app->request->queryParams['LoansSearch']['term'];
		}
		
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'statusParams' => $statusParams,
			'termParams' => $termParams,
			'model' => $model,
		]);
		
    }

    public function actionView($id)
    {
		$model =  $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }

	public function actionApprove($id)
    {

        $model = $this->findModel($id);
	
		$messageText = "Your loan has been approved.";
        if ($model->load(Yii::$app->request->post()) && $model->save(false) && $model->sendApproveLoanNotif() && $model->sendSms($messageText,$model->customer->phone_number)) {
			echo json_encode([
				'response'=>true,
				'message'=>"Success"
				
			]);
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}

       
    }
	
	public function actionDecline($id)
    {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->declineLoan()) {
			echo json_encode([
				'response'=>true,
				'message'=>"Success"
				
			]);
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}

       
    }
	
	public function actionAsk($id)
    {

        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post()) && $model->askDetails()) {
			echo json_encode([
				'response'=>true,
				'message'=>"Success"
				
			]);
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}

       
    }
    protected function findModel($id)
    {
        if (($model = Loans::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
}
