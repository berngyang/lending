<?php

namespace backend\controllers;

use Yii;
use common\models\Customers;
use backend\models\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CustomerController extends Controller
{
	const SUPER_ADMIN = 1;
	const ADMIN = 2;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
		$model = new Customers();

		$searchModel = new CustomersSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 15];
		$statusParams = "";
		
		if(isset(Yii::$app->request->queryParams['CustomersSearch']['status'])) {
			$statusParams = Yii::$app->request->queryParams['CustomersSearch']['status'];
		}
		
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'statusParams' => $statusParams,
			'model' => $model,
		]);
		
    }

    public function actionView($id)
    {
		
		$model =  $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$addresses = $model->getUserAddress();

		
        if ($model->load(Yii::$app->request->post()) && $model->userUpdate($addresses )) {
			Yii::$app->session->setFlash('success',  "User has been updated!");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'addresses' => $addresses,
        ]);
    }
	
	public function actionActivate($id)
    {

        $model = $this->findModel($id);
	
	
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			echo json_encode([
				'response'=>true,
				'message'=>"Success"
				
			]);
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}

       
    }
	public function actionApproveRegistration($id)
    {

        $model = $this->findModel($id);
	
        if ($model->approveRegistration()) {
			
			Yii::$app->session->setFlash('success',  "Registration approval has been successful.");
        } else {
			Yii::$app->session->setFlash('error',  "Failed to approve registration!");
		}
		return $this->redirect(['view', 'id' => $model->id]);

       
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	public function actionFlushOldData()
    {		
		$date = date("Y-m-d", strtotime("-6 months"));
		$qrCodes = QrCodes::find()->where(['<','created',$date])->all();		
		$pumRelatedDocs = PumRelatedDocs::find()->where(['<','created',$date])->all();		
		$fileUploads = FileUploads::find()->where(['<','created',$date])->all();		
		TemperaturesCollections::deleteAll(['<','created',$date]);		
		Temperatures::deleteAll(['<','created',$date]);		
		// Scanned::deleteAll(['<','created',$date]);		
		PumFindings::deleteAll(['<','created',$date]);		
		Pum::deleteAll(['<','created',$date]);		
		CloseContacts::deleteAll(['<','created',$date]);		
		CloseContactsCollections::deleteAll(['<','created',$date]);		
		Checklists::deleteAll(['<','created',$date]);		
		ChecklistCollections::deleteAll(['<','created',$date]);		
		
		foreach($qrCodes as $qrCode){
			$path = Yii::getAlias('@frontend/web/qrcodes/'.$qrCode->uuid.".png");
			if (file_exists($path) && $qrCode->uuid != "") {
				unlink($path);
			}
			$qrCode->delete();
		}
		foreach($pumRelatedDocs as $pumRelatedDoc){
			$path = Yii::getAlias('@backend/web/uploads/pums/'.$pumRelatedDoc->filename);
			if (file_exists($path) && $pumRelatedDoc->filename != "") {
				unlink($path);
			}
			$pumRelatedDoc->delete();
		}
		
		foreach($fileUploads as $fileUpload){
			$path = Yii::getAlias('@backend/web/uploads/checklists/'.$fileUpload->filename);
			if (file_exists($path) && $fileUpload->filename != "") {
				unlink($path);
			}
			$fileUpload->delete();
		}
		
		
		
        return true;
		
    }
}
