<?php

use yii\db\Migration;

/**
 * Class m210418_080605_add_new_field_to_user
 */
class m210418_080605_add_new_field_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210418_080605_add_new_field_to_user cannot be reverted.\n";

        return false;
    }
	public function up()
    {
		 $this->addColumn('{{%user}}', 'school_id', Schema::TYPE_STRING);
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210418_080605_add_new_field_to_user cannot be reverted.\n";

        return false;
    }
    */
}
