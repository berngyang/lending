<?php

use yii\db\Migration;

class m210609_134237_create_table_questions extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%questions}}',
            [
                'id' => $this->primaryKey(),
                'title' => $this->string(50),
                'question' => $this->string(),
                'status' => $this->tinyInteger(4)->defaultValue('2'),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%questions}}');
    }
}
