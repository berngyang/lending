<?php

use yii\db\Migration;

class m210609_134344_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%user}}',
            [
                'id' => $this->primaryKey(),
                'school_id' => $this->string(50),
                'username' => $this->string()->notNull(),
                'first_name' => $this->string()->notNull(),
                'last_name' => $this->string()->notNull(),
                'auth_key' => $this->string(32)->notNull(),
                'password_hash' => $this->string()->notNull(),
                'password_reset_token' => $this->string(),
                'email' => $this->string()->notNull(),
                'account_type' => $this->tinyInteger(4),
                'status' => $this->smallInteger()->notNull()->defaultValue('0'),
                'verification_token' => $this->string(),
                'address_id' => $this->integer(),
                'college_id' => $this->integer(),
                'course_id' => $this->integer(),
                'department_id' => $this->integer(),
                'role_id' => $this->integer(),
                'birthday' => $this->date(),
                'gender' => $this->string(10),
                'mobile_number' => $this->string(50),
                'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            ],
            $tableOptions
        );

        $this->createIndex('password_reset_token', '{{%user}}', ['password_reset_token'], true);
        $this->createIndex('email', '{{%user}}', ['email'], true);
        $this->createIndex('username', '{{%user}}', ['username'], true);

        $this->addForeignKey(
            'address',
            '{{%user}}',
            ['address_id'],
            '{{%addresses}}',
            ['id'],
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            'college',
            '{{%user}}',
            ['college_id'],
            '{{%colleges}}',
            ['id'],
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            'course',
            '{{%user}}',
            ['course_id'],
            '{{%courses}}',
            ['id'],
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            'department',
            '{{%user}}',
            ['department_id'],
            '{{%departments}}',
            ['id'],
            'RESTRICT',
            'RESTRICT'
        );
        $this->addForeignKey(
            'role',
            '{{%user}}',
            ['role_id'],
            '{{%roles}}',
            ['id'],
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
