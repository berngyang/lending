<?php

use yii\db\Migration;

class m210609_134142_create_table_pum extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%pum}}',
            [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'start_date' => $this->date()->defaultValue('curdate()'),
                'case_type' => $this->string(50),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%pum}}');
    }
}
