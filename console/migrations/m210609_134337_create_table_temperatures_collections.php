<?php

use yii\db\Migration;

class m210609_134337_create_table_temperatures_collections extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%temperatures_collections}}',
            [
                'id' => $this->primaryKey(),
                'temperature_id' => $this->integer(),
                'temperature_am' => $this->string(50)->defaultValue('0'),
                'temperature_pm' => $this->string(50)->defaultValue('0'),
                'day' => $this->integer(),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%temperatures_collections}}');
    }
}
