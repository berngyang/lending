<?php

use yii\db\Migration;

class m210609_133656_create_table_addresses extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%addresses}}',
            [
                'id' => $this->primaryKey(),
                'street' => $this->string(50),
                'barangay' => $this->string(50),
                'city' => $this->string(50),
                'province' => $this->string(50),
                'post_code' => $this->string(50),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'updated' => $this->dateTime(),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%addresses}}');
    }
}
