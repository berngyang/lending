<?php

use yii\db\Migration;

class m210609_133740_create_table_close_contacts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%close_contacts}}',
            [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'pum_id' => $this->integer(),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%close_contacts}}');
    }
}
