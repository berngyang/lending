<?php

use yii\db\Migration;

class m210609_133715_create_table_checklist_collections extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%checklist_collections}}',
            [
                'id' => $this->primaryKey(),
                'checklist_id' => $this->integer(),
                'answer' => $this->tinyInteger(4),
                'question_id' => $this->integer(),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->date()->defaultValue('curdate()'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );

        $this->createIndex('answer_id', '{{%checklist_collections}}', ['checklist_id']);
        $this->createIndex('question_id', '{{%checklist_collections}}', ['question_id']);
    }

    public function down()
    {
        $this->dropTable('{{%checklist_collections}}');
    }
}
