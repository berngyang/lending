<?php

use yii\db\Migration;

class m210609_134309_create_table_scanned extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%scanned}}',
            [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'status' => $this->tinyInteger(4)->notNull()->defaultValue('0'),
                'visit_date' => $this->integer(),
                'gate' => $this->tinyInteger(4),
                'updated' => $this->dateTime(),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%scanned}}');
    }
}
