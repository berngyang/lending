<?php

use yii\db\Migration;

/**
 * Class m210609_132841_migration_09_06_2021
 */
class m210609_132841_migration_09_06_2021 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210609_132841_migration_09_06_2021 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210609_132841_migration_09_06_2021 cannot be reverted.\n";

        return false;
    }
    */
}
