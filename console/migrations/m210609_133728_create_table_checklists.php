<?php

use yii\db\Migration;

class m210609_133728_create_table_checklists extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%checklists}}',
            [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer(),
                'file_upload_id' => $this->integer(),
                'answer_date' => $this->date()->defaultValue('curdate()'),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );

        $this->createIndex('file_upload_id', '{{%checklists}}', ['file_upload_id']);
        $this->createIndex('user_id', '{{%checklists}}', ['user_id']);
    }

    public function down()
    {
        $this->dropTable('{{%checklists}}');
    }
}
