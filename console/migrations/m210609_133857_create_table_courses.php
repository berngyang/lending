<?php

use yii\db\Migration;

class m210609_133857_create_table_courses extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%courses}}',
            [
                'id' => $this->primaryKey(),
                'college_id' => $this->integer(),
                'name' => $this->string(100)->notNull(),
                'code' => $this->string(50)->notNull(),
                'description' => $this->string(100)->notNull(),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'updated' => $this->dateTime(),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%courses}}');
    }
}
