<?php

use yii\db\Migration;

class m210609_134211_create_table_pum_related_docs extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%pum_related_docs}}',
            [
                'id' => $this->primaryKey(),
                'pum_id' => $this->integer()->notNull(),
                'filename' => $this->string(),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%pum_related_docs}}');
    }
}
