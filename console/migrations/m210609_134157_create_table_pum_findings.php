<?php

use yii\db\Migration;

class m210609_134157_create_table_pum_findings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%pum_findings}}',
            [
                'id' => $this->primaryKey(),
                'physician_id' => $this->integer(),
                'pum_id' => $this->integer(),
                'summary' => $this->string(),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'pum_status' => $this->string(50),
                'created' => $this->date()->defaultValue('curdate()'),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%pum_findings}}');
    }
}
