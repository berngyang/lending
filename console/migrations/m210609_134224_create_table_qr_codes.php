<?php

use yii\db\Migration;

class m210609_134224_create_table_qr_codes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%qr_codes}}',
            [
                'id' => $this->primaryKey(),
                'uuid' => $this->string(50)->defaultValue('uuid()'),
                'user_id' => $this->integer(),
                'checklist_id' => $this->integer(),
                'status' => $this->tinyInteger(4)->defaultValue('0'),
                'created' => $this->date()->defaultValue('curdate()'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%qr_codes}}');
    }
}
