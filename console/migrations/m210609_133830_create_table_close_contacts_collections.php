<?php

use yii\db\Migration;

class m210609_133830_create_table_close_contacts_collections extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%close_contacts_collections}}',
            [
                'id' => $this->primaryKey(),
                'close_contacts_id' => $this->integer(),
                'user_id' => $this->integer(),
                'time_code' => $this->tinyInteger(4),
                'status' => $this->tinyInteger(4)->defaultValue('1'),
                'created' => $this->date()->defaultValue('curdate()'),
                'updated' => $this->dateTime(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%close_contacts_collections}}');
    }
}
