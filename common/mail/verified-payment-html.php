<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$link = Yii::$app->urlManager->createAbsoluteUrl(['loan/view', 'id' => $loan->id]);
?>
<div class="verified-payment">
    <p>Hello <?= Html::encode($loan->customer->fullName) ?>,</p>

    <p>Payment has been verified:</p>

    <p><?= Html::a(Html::encode($link), $link) ?></p>
</div>
