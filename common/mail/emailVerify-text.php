<?php

/* @var $this yii\web\View */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $customer->verification_token]);
?>
Hello <?= $customer->username ?>,

Follow the link below to verify your email:

<?= $verifyLink ?>
