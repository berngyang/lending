<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$link = 'www.lending-loan.com';
?>
<div class="verified-payment">
    <p>Hello <?= Html::encode($model->fullName) ?>,</p>

    <p>Your account has been approved you can now to log in using your account:</p>

    <p><?= Html::a(Html::encode($link), $link) ?></p>
</div>
