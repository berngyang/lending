<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$link = 'www.lending-loan.com/loan/view?id='.$loan->id;
?>
<div class="verified-payment">
    <p>Hello <?= Html::encode($loan->customer->fullName) ?>,</p>

    <p>Your loan has been Reviewed. Please check here the complete details:</p>

    <p><?= Html::a(Html::encode($link), $link) ?></p>
</div>
