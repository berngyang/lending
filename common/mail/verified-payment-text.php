<?php

/* @var $this yii\web\View */

$link = Yii::$app->urlManager->createAbsoluteUrl(['loan/view', 'id' => $loan->id]);
?>
Hello <?= $loan->customer->fullName ?>,

Payment has been verified:

<?= $link ?>
