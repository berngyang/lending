<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $customer->verification_token]);
?>
<div class="verify-email">
    <p>Hello <?= Html::encode($customer->username) ?>,</p>

    <p>Follow the link below to verify your email:</p>

    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
</div>
