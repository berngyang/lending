<?php

namespace common\models;

use Yii;

class GcashAccounts extends \yii\db\ActiveRecord
{
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	const STATUS_DONE = 4;
	const STATUS_OVER_DUE = 5;
	const STATUS_CANCELLED = 6;
	const STATUS_DECLINED = 7;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gcash_accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date','status'], 'safe'],
            ['phone_number', 'required'],
            ['full_name', 'required'],
            ['customer_id', 'required'],
            ['email', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone_number' => 'Phone number',
            'full_name' => 'Full Name',
            'customer_id' => 'Customer',
            'email' => 'Email',
        ];
    }
	
	public function getStatusText()
	{
		$statusLabel = [
			self::STATUS_ACTIVE => "<labe class='badge badge-success'>Active</label>",
			self::STATUS_INACTIVE => "<labe class='badge badge-warning'>Inactive</label>",
		];
		
		return $statusLabel[$this->status];
	}
}
