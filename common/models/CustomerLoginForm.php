<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class CustomerLoginForm extends Model
{
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	
    public $username;
    public $password;
    public $rememberMe = true;
	
    private $_customer;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $customer = $this->getUser();

            if (!$customer || !$customer->validatePassword($this->password)) {

                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
			if($this->validateActiveUser()) {
				return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
			} else {
				 Yii::$app->session->setFlash('error', 'Your account is invalid. Please contact administrator for help.');
			}
			
           
        }
        
        return false;
    }
	

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
	 
	protected function validateActiveUser()
	{
		
		return Customers::findByUsername($this->username)->status == self::STATUS_ACTIVE;
	}
	
    protected function getUser()
    {
        if ($this->_customer === null) {
            $this->_customer = Customers::findByUsername($this->username);
        }

        return $this->_customer;
    }
}
