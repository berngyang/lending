<?php
namespace common\models;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use app\models\Loans;
use app\models\Employments;
use common\models\Addresses;
use common\models\GcashAccounts;
use app\models\CustomerDocuments;

use MessageBird\Client;
use MessageBird\Objects\Message;

class Customers extends ActiveRecord implements IdentityInterface
{
    const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	
	const CIVIL_SINGLE = 1;
	const CIVIL_MARRIED = 2;
	const CIVIL_DIVORCE = 3;
	const CIVIL_SEPARATED = 4;
	const CIVIL_WIDOWED = 5;
	
    const SOURCE_SELF_EMPLOYED = 1;
	const SOURCE_PRIVATE = 2;
	const SOURCE_GOVERNMENT = 3;
	const SOURCE_BUSINESS_OWNER = 4;
	const SOURCE_OFW = 5;
	
	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;
	
	const USER_STATUS = [
		0 => '<span class="badge badge-primary">Pending</span>',
		1 => '<span class="badge badge-success">Active</span>',
		2 => '<span class="badge badge-warning">Inactive</span>',
		3 => '<span class="badge badge-danger">Deleted</span>'
	];


	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%customers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds customers by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['customers.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	
	public static function getStatus()

    {

        return array(

            "" => 'Select Status',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted',

            );

    }
	
	public static function getStatuses()
    {

        return array(

            "" => 'Select Status',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_ACTIVE => 'Active',
            // self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted',

            );

    }
	
	
	public function loadAttributes(Customers $customer)
    {
        // here is the magic happens
        $customer->setAttributes([
            'email'    => $this->email,
            'username' => $this->username,
            'password' => $this->password
        ]);
    
    }
	
	public function beforeSave($insert) 
	{

		$time = strtotime($this->birthday);

		$time = date("Y-m-d", strtotime($this->birthday));

		$this->birthday = $time;

		return parent::beforeSave($insert);

		

	}


	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'signature' => 'Signature',
            'verification_token' => 'Verification Token',
            'employment_id' => 'Employment ID',
            'address_id' => 'Address ID',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'phone_number' => 'Phone Number',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	
	public function getAllPendingRegistrations()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_PENDING
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	
	public function getAllActiveUsers()
	{
		
		return Customers::find(['status'=>self::STATUS_ACTIVE]) ->andWhere(['<>','id',$this->id])->all();
	
	}
	
	public function getFullName()
	{

			return $this->first_name.' '.$this->last_name;

	}
	public function getGenderText()
	{
		$gender=[
			self::GENDER_MALE =>'Male', 
			self::GENDER_FEMALE =>'Female', 
		];
		return $gender[$this->gender];

	}
	public function getCivilStatus()
	{
		$civilStatus = [
			self::CIVIL_SINGLE => 'Single',
			self::CIVIL_MARRIED => 'Married',
			self::CIVIL_DIVORCE => 'Divorce',
			self::CIVIL_SEPARATED => 'Separated',
			self::CIVIL_WIDOWED => 'Widowed',
		];
		return $civilStatus[$this->civil_status];

	}
	public function getSourceOfIncome()
	{
		$sourceOfIncome = [
			self::SOURCE_SELF_EMPLOYED => 'Self Employed',
			self::SOURCE_PRIVATE => 'Private Employee',
			self::SOURCE_GOVERNMENT => 'Government Employee',
			self::SOURCE_BUSINESS_OWNER => 'Business Owner',
			self::SOURCE_OFW => 'OFW',
		];
		return $sourceOfIncome[$this->employments->source_of_income];

	}
	public function getAddress()
    {
        return $this->hasOne(Addresses::className(), ['id' => 'address_id']);
    }
	public function getEmployments()
    {
        return $this->hasOne(Employments::className(), ['id' => 'employment_id']);
    }
	public function getDocuments()
    {
        return $this->hasOne(CustomerDocuments::className(), ['id' => 'document_id']);
    }
	public function getLoans()
    {
        return $this->hasMany(Loans::className(), ['customer_id' => 'id']);
    }
	public function getGcashAccount()
    {
        return $this->hasOne(GcashAccounts::className(), ['customer_id' => 'id']);
    }
	public function currentUserDetails()
	{
		
		return Customers::find(Yii::$app->user->identity->id)->one();
	
	}
	public function getUserAddress()
    {
		return Addresses::findOne($this->address_id);
    }
	public function getUserEmployment()
    {
		return Employments::findOne($this->employment_id);
    }
	public function customerUpdate($address, $employments)
	{
		
		// if($this->new_password != "") {
			// $this->setPassword($this->new_password);
		// }

		if ($this->validate() == false && $address->validate() == false && $employments->validate() == false) {

            return null;
        }
		
        return $this->save() && $address->save() && $employments->save();
	}
	
	public function approveRegistration()
	{
		$this->status = self::STATUS_ACTIVE;
		$messageText = "Your account has been approved you can now to log in using your account";
        return $this->save(false) && $this->sendApproveRegistrationNotif() && $this->sendSms($messageText,$this->phone_number);
	}
	
	protected function sendApproveRegistrationNotif()
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'approve-registration-html', 'text' => 'approve-registration-text'],
                ['model' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Registration Approved ' . Yii::$app->name)
            ->send();
    }
	
	public function sendSms($messageText,$number)
	{
		$number = $this->formatPhoneNumber($number);
		$messagebird = new Client('GP74bHPuYXstJjeGiHl7bE5mV');//TEST
		// $messagebird = new Client('ESIFreO1NoJfTLQDwcniTr4Hg');//PROD
		$message = new Message;
		$message->originator = '+639187856241';
		// $message->recipients = [ $number];
		$message->recipients = [ '+639187856241'];//PROD when upgraded
		$message->body = $messageText;
		$response = $messagebird->messages->create($message);
		return true;
	}
	
	public function formatPhoneNumber($number)
	{
		if($number[0] == "0"){
			return str_replace("+6", "0", $number);;
		} else if($number[0] == "6") {
			return "+".$number;
		}
		
		return $number;
	}
}
