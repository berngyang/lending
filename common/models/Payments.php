<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use app\models\Loans;
class Payments extends \yii\db\ActiveRecord
{
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	const STATUS_DONE = 4;
	const STATUS_OVER_DUE = 5;
	const STATUS_CANCELLED = 6;
	const STATUS_DECLINED = 7;
	
	const PUBLIC_KEY = 'pk_test_c0e7bdc2c014a55fa9bf7feb4b87fef5';
	
	const PAYMENT_STATUS_PENDING = 0;
	const PAYMENT_STATUS_PAID = 1;
	const PAYMENT_STATUS_DUE = 2;
	const PAYMENT_STATUS_DECLINED = 3;
	
	const PAYMENT_TYPE_GCASH = 1;
	const PAYMENT_TYPE_REMITTANCE = 2;
    
	public $imageFile;
	
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date','remarks','slip','payment_type','request_id','payment_date','payment_status','status'], 'safe'],
            ['loan_id', 'required'],
            ['amount', 'required'],
            ['customer_id', 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'loan_id' => 'Loan',
            'amount' => 'Amount',
            'customer_id' => 'Customer',
            'request_id' => 'Request ID',
            'remarks' => 'Remarks',
        ];
    }
	
	
	public function createPaymentRequest($loan)
	{
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://g.payx.ph/payment_request',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => array(
			'x-public-key' => 'pk_test_c0e7bdc2c014a55fa9bf7feb4b87fef5',
			'amount' => $loan->amortization,
			'description' => 'Payment for loan'
		  ),
		));

		$response = json_decode(curl_exec($curl));

		curl_close($curl);
		$model = new Payments;
		$model->loan_id = $loan->id;
		$model->amount = $loan->amortization;
		$model->customer_id = $loan->customer_id;
		$model->request_id = $response->data->request_id;
		// $model->fee = $response->data->fee;
		// $model->description = $response->data->description;
		// $model->expiry = $response->data->expiry;
		$model->checkout_url = $response->data->checkouturl;
		$model->save(false);
		return $model;
	}
	public function removePayment()
	{
		$this->status = self::STATUS_DELETED;
		return $this->save(false);
	}
	public function getButton()
	{
		
		$button = '<a href="'.$this->checkout_url.'" target="_blank">
					<img src="https://getpaid.gcash.com/assets/img/paynow.png">
					</a>';
		return $button;
	}
	
	public function getPaymentStatusText()
	{
		$statusLabel = [
			self::PAYMENT_STATUS_PENDING => "<labe class='badge badge-warning'>PAYMENT PENDING</label>",
			self::PAYMENT_STATUS_PAID => "<labe class='badge badge-success'>PAYMENT PAID</label>",
			self::PAYMENT_STATUS_DUE => "<labe class='badge badge-danger'>PAYMENT DUE</label>",
		];
		
		return $statusLabel[$this->payment_status];
	}
	
	public function getPaymentTypeText()
	{
		$paymentType = [
			self::PAYMENT_TYPE_GCASH => "<labe class='badge badge-primary'>GCASH</label>",
			self::PAYMENT_TYPE_REMITTANCE => "<labe class='badge badge-success'>Remittance</label>",
		];
		
		return $paymentType[$this->payment_type];
	}
	
	public function addRemittanceSlip()
	{
		
		$this->imageFile = UploadedFile::getInstance($this, 'imageFile');
		$this->payment_type = self::PAYMENT_TYPE_REMITTANCE;
		$this->customer_id = Yii::$app->user->identity->id;
		
		if($this->imageFile != null) {				
			
			if(!file_exists(Yii::getAlias('@backend').'/web/uploads/customer/remittance/')) {
				mkdir(Yii::getAlias('@backend').'/web/uploads/customer/remittance/',0777,true);
			}
			if(!file_exists(Yii::getAlias('@frontend').'/web/uploads/customer/remittance/')) {
				mkdir(Yii::getAlias('@frontend').'/web/uploads/customer/remittance/',0777,true);
			}
			$path = "";
			if ($this->validate()) {
				$path = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
				$this->imageFile->saveAs(Yii::getAlias('@frontend').'/web/uploads/customer/remittance/' .$path );
				$copied = copy(Yii::getAlias('@frontend').'/web/uploads/customer/remittance/' .$path  ,Yii::getAlias('@backend').'/web/uploads/customer/remittance/' .$path );
			} else {
				return false;
			}
			
			$this->slip = $path ;
			
			return $this->save(false);
			
			
		}
		
		return false;
		
		
	}
	
	public function verifyPayment()
	{
		$loan = Loans::findOne($this->loan_id);
		$loan->total_paid = $loan->total_paid + $this->amount;
		
		if($loan->total_paid >= $loan->total_due) {
			$loan->status = $loan::STATUS_DONE;
		}
		$this->payment_date =  date("Y-m-d h:i:sa");
		$this->status =  self::STATUS_DONE;
		
		return $this->save(false) && $loan->save(false) && $this->sendVerificationNotif($loan);
	}
	
	protected function sendVerificationNotif($loan)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'verified-payment-html', 'text' => 'verified-payment-text'],
                ['loan' => $loan]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($loan->customer->email)
            ->setSubject('Payment Verified ' . Yii::$app->name)
            ->send();
    }
}
