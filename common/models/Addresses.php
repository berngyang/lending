<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property int $id
 * @property string|null $street
 * @property string|null $city
 * @property string|null $province
 * @property string|null $post_code
 * @property string|null $status
 * @property string|null $updated
 * @property string|null $created
 */
class Addresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['updated', 'created'], 'safe'],
            [['street','barangay', 'city', 'province', 'post_code'], 'string', 'max' => 50],
            ['barangay', 'required'],
            ['street', 'required'],
            ['city', 'required'],
            ['province', 'required'],
            ['post_code', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'street' => 'Street',
            'barangay' => 'Barangay',
            'city' => 'City',
            'province' => 'Province',
            'post_code' => 'Post Code',
            'status' => 'Status',
            'updated' => 'Updated',
            'created' => 'Created',
        ];
    }
	
	
	
	public static function getCompleteAddressByUserID($userID)
	{
		
		$address = Addresses::find()->where(

			array(
				'user_id'=>$userID,
				'status=1'
			)

		);
		$completeAddress = "";
		$varAddress = new ActiveDataProvider(['address'=> $address]);
		var_dump($varAddress);
		if($address != null) {
			
			//$completeAddress = $address[0];
			
		}
		
		
		return $completeAddress;
	}
}
