<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id= "sign-up-customer-page" class="site-signup">

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => 'signup-form']); ?>
				
				
				
				 
				<div class="basic-info form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Basic Information</h4>
						</div>	
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'first_name') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'middle_name') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'last_name') ?>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'email') ?>
						</div>
						
						<div class="col-md-4">
							<?= $form->field($model, 'phone_number') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'civil_status')
									 ->dropDownList(
											array(
											""=>"Select Civil Status",
											1=>"Single",
											2=>"Married",
											3=>"Divorced",
											4=>"Separated",
											5=>"Widowed"
											)
										)
							?>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'gender')
									 ->dropDownList(
											array(
											""=>"Select Gender",
											1=>"Male",
											2=>"Female"
											)
										)
							?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'age')->textInput(['type' => 'number']) ?>
						</div>
						<div class="col-md-2">
							
							<?= $form->field($model,'birthday')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options'=>['class'=>'form-control','placeholder'=>'yyyy-MM-dd']]) ?>
						</div>
						
					</div>	
				</div>	
				<div class="address-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Address</h4>
						</div>	
					</div>	
					
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'house_number') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'street') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'barangay') ?>
						</div>
						
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'city') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'province') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'post_code') ?>
						</div>
					</div>	
				</div>	
				<div class="employment-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Employment Details</h4>
						</div>	
					</div>	
					<div class="row">
						
						<div class="col-md-4">
							<?= $form->field($model, 'source_of_income')
								 ->dropDownList(
										array(
										""=>"Select Source of Income",
										1=>"Self Employed",
										2=>"Private Employee",
										3=>"Government Employee",
										4=>"Business Owner",
										5=>"OFW",
										)
									)
							?>
						</div>
					</div>	
					<div class="row income hide-all-display">
						
						<div class="col-md-4">
							<?= $form->field($model, 'monthly_income')->textInput(['type' => 'number']) ?>
						</div>
						<div class="col-md-4 employed">
							<?= $form->field($model, 'occupation') ?>
						</div>
						
					</div>	
				</div>	
				<div class="documents-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Documents</h4>
						</div>	
					</div>	
					<div class="row ">
						<div class="col-md-12">
							<?= $form->field($model, 'valid_id')->fileInput() ?>
						</div>
						<div class="col-md-12">
							<?= $form->field($model, 'secondary_id')->fileInput() ?>
						</div>
						<div class="col-md-12">
							<?= $form->field($model, 'proof_of_billing')->fileInput() ?>
						</div>
					 </div>		
				</div>	
				<div class="terms-and-conditions-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Terms and Conditions</h4>
						</div>	
					</div>	
					<div class="row ">
						<div class="col-md-12">
							<p>

								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer varius dui id nisi accumsan sagittis. Duis blandit, nisi maximus sodales dapibus, 
								magna orci fermentum orci, sit amet vulputate justo est ac ex. Morbi feugiat ex vel vestibulum fringilla. Sed finibus, tortor et bibendum ornare, 
								ex ante ultricies risus, eu eleifend lorem nunc id quam. In laoreet dapibus augue, a placerat est porttitor id. Vivamus dapibus vestibulum nisi id interdum. 
								Vivamus posuere cursus euismod.
							</p>
						</div>	
						<div class="col-md-12">
							<p>
								Nam gravida accumsan egestas. Praesent massa purus, euismod vel placerat a, eleifend a nibh. Praesent nec vulputate elit. Mauris at erat condimentum, 
								vulputate nisl sit amet, maximus nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec semper dolor diam, 
								ut ornare neque interdum eu. Duis vestibulum ante quis justo aliquet, non ullamcorper arcu hendrerit. In vehicula tempor efficitur. 
								In hac habitasse platea dictumst. In turpis urna, pellentesque tempus blandit vel, accumsan sed arcu. Suspendisse potenti. 
								Nullam suscipit sagittis justo in tempor. Morbi in consectetur sapien.
							</p>
						</div>
						<div class="col-md-12">
							<p>
								Nam eleifend tortor augue, viverra bibendum mi pharetra eu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
								Vivamus et ante tincidunt, consectetur odio ac, molestie neque. Duis vel sem ac purus iaculis lobortis iaculis eu tellus. Integer ultricies ut sem id placerat. 
								Nullam consequat efficitur lacus ac pharetra. Nullam neque leo, ultrices sit amet odio in, faucibus venenatis erat. Integer ornare efficitur velit nec convallis. 
								Mauris tristique sit amet velit at maximus. Maecenas placerat, erat sit amet interdum convallis, sem risus imperdiet lorem, ac maximus purus elit sed velit. 
								Nullam tincidunt urna non diam dictum, eu gravida neque porta.
							</p>
						</div>
						<div class="col-md-12">
							<p>
								Donec eget orci in sem bibendum viverra. Suspendisse condimentum commodo elit quis luctus. Donec et justo nec risus porta semper. Quisque ullamcorper, 
								est nec ultrices accumsan, lectus magna convallis quam, quis imperdiet orci justo quis erat. Curabitur egestas tempor sem, sit amet blandit sapien ullamcorper at. 
								Curabitur auctor a mi vel porttitor. Pellentesque eu tincidunt nulla. Phasellus dapibus nisl vel cursus gravida.
							</p>
						</div>
						<div class="col-md-12">
							<p>
								Maecenas eget facilisis ex. Mauris in mauris nec neque efficitur eleifend a vel enim. Fusce feugiat suscipit nunc id molestie. 
								Praesent erat ex, pellentesque a faucibus eu, iaculis non enim. Morbi suscipit egestas volutpat. Fusce elementum, ligula quis pretium semper, 
								mi risus malesuada lectus, commodo blandit turpis ipsum vitae libero. Maecenas elementum leo ante, ut posuere turpis feugiat ullamcorper. 
								Etiam fringilla turpis in nulla ullamcorper condimentum sed et lorem. Aliquam eu arcu et dolor congue placerat. 
								Morbi nec est vitae turpis consectetur sollicitudin. Nulla at libero a purus iaculis accumsan. Nunc non nisi metus. 
								Vestibulum facilisis lectus ac odio tempus placerat. Pellentesque a turpis sit amet est condimentum pellentesque sit amet sit amet lectus. 
								Nulla facilisi. Nunc eu leo consectetur, tristique neque vitae, rhoncus urna. 
							</p>
						</div>
					</div>
					<div class="row  ">
						<div class="col-md-6 col-md-offset-3">
							<h3 class="tag-info">Put signature below:</h3>
							<?= $form->field($model, 'signature')->hiddenInput()->label(false); ?>
							<div id="signArea" >
								
								<div class="sig sigWrapper" style="height:auto;">
									<div class="typed"></div>
									<canvas class="sign-pad" id="sign-pad" width="550" height="200"></canvas>
								</div>
							</div>
						 </div>	
					 </div>	
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<a href="javascript:void(0)" class="btn btn-default clear-pad">Reset</a>
						</div>		
					 </div>		
				</div>	
				<div class="credential-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Credentials</h4>
						</div>	
					</div>	
					<div class="row ">
						<div class="col-md-4">
							<?= $form->field($model, 'username')->textInput() ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'password')->passwordInput() ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'confirm_password')->passwordInput() ?>
						</div>
					 </div>		
				</div>	
				
				<div class="basic-info-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-primary pull-right next-address" > Next </a>
						</div>
					</div>
                </div>
				<div class="address-info-btn hide-all-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-basic" > Previous </a>
							<a href="javascript:void(0)" class="btn btn-primary pull-right employment-next" > Next </a>
						</div>
					</div>
                </div>
				<div class="employment-info-btn hide-all-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-address" > Previous </a>
							<a href="javascript:void(0)" class="btn btn-primary pull-right next-documents" > Next </a>
						</div>		
					</div>	
                </div>
				<div class="documents-info-btn hide-all-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-employment" > Previous </a>
							<a href="javascript:void(0)" class="btn btn-primary pull-right next-terms-and-conditions" > Next </a>
						</div>		
					</div>	
                </div>
				<div class="terms-and-conditions-info-btn hide-all-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-documents" > Previous </a>
							<a href="javascript:void(0)" class="btn btn-primary pull-right next-credential" > Next </a>
						</div>		
					</div>	
                </div>				
				<div class="credential-info-btn hide-all-btn">				
					<div class="row">				
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-terms-and-conditions" > Previous </a>
							<?= Html::submitButton('Signup', ['class' => 'btn btn-primary pull-right', 'name' => 'signup-button']) ?>
						</div>
					</div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

