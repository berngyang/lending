<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Customers;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="eHealth-nav">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'About Us', 'url' => ['/site/about-us']];
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
		$menuItems[] = ['label' => 'About Us', 'url' => ['/site/about-us']];
		$menuItems[] = ['label' => 'Profile', 'url' => ['#'],
				'items' =>[
					
					['label' => 'My Account', 'url' => ['/customer/my-account']],
					['label' => 'Loan', 'url' => ['/customer/loan']],
					// ['label' => 'Log out', 'url' => ['/site/logout']],
					
				]
				
				];
		
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Log out',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
	
    ?>

</div>
<?php if(Yii::$app->request->url == "/" || Yii::$app->request->url == "/site/index") { ?>
	<div class="wrap bg-facade"></div> 
	<div class="div-logo">
		<div><?php echo Html::img('@web/images/lending.png', ['alt'=>'lending_logo', 'class'=>'lending_logo']) ?></div>
		<span class="lending-logo">Lending</span>
	</div>

<?php } ?>
	<div class="container main-container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>
		<?= $content ?>
	</div>
</div>


<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <a href="#">Lending <?= date('Y') ?> </a></p>

        <p class="pull-right">
           
           
           <a href="#" target="_blank" rel="noopener"><i class="fab fa-facebook"></i></a> 
           <a href="#" target="_blank" rel="noopener"><i class="fab fa-twitter"></i></a> 
           <a href="#" target="_blank" rel="noopener"><i class="fab fa-google-plus"></i></a>
        </p>
        
    </div>
</footer>

<?php $this->endBody() ?>
	
</body>
</html>
<?php $this->endPage() ?>
