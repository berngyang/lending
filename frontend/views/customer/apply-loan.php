<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Apply Loan' ;
$this->params['breadcrumbs'][] = 'Apply Loan';
?>
<div class="apply-loan">


    <?= $this->render('_loan', [
        'model' => $model
    ]) ?>

</div>
