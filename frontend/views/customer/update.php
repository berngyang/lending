<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

$this->title = 'Update account';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="customer-update-page" class="site-update">

    <p>Please fill out the following fields to update account:</p>

    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'customer-form']); ?>
				
				
				
				 
				<div class="basic-info form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Basic Information</h4>
						</div>	
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'first_name') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'middle_name') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'last_name') ?>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'email') ?>
						</div>
						
						<div class="col-md-4">
							<?= $form->field($model, 'phone_number') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'civil_status')
									 ->dropDownList(
											array(
											""=>"Select Civil Status",
											1=>"Single",
											2=>"Married",
											3=>"Divorced",
											4=>"Separated",
											5=>"Widowed"
											)
										)
							?>
						</div>
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($model, 'gender')
									 ->dropDownList(
											array(
											""=>"Select Gender",
											1=>"Male",
											2=>"Female"
											)
										)
							?>
						</div>
						<div class="col-md-4">
							<?= $form->field($model, 'age')->textInput(['type' => 'number']) ?>
						</div>
						<div class="col-md-2">
							
							<?= $form->field($model,'birthday')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options'=>['class'=>'form-control','placeholder'=>'yyyy-MM-dd']]) ?>
						</div>
						
					</div>	
				</div>	
				<div class="address-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Address</h4>
						</div>	
					</div>	
					
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($addresses, 'house_number') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($addresses, 'street') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($addresses, 'barangay') ?>
						</div>
						
					</div>	
					<div class="row">
						<div class="col-md-4">
							<?= $form->field($addresses, 'city') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($addresses, 'province') ?>
						</div>
						<div class="col-md-4">
							<?= $form->field($addresses, 'post_code') ?>
						</div>
					</div>	
				</div>	
				<div class="employment-info hide-all-form form-group">
					<div class="row">
						<div class="col-md-6">
							<h4>Employment Details</h4>
						</div>	
					</div>	
					<div class="row">
						
						<div class="col-md-4">
							<?= $form->field($employments, 'source_of_income')
								 ->dropDownList(
										array(
										""=>"Select Source of Income",
										1=>"Self Employed",
										2=>"Private Employee",
										3=>"Government Employee",
										4=>"Business Owner",
										5=>"OFW",
										)
									)
							?>
						</div>
					</div>	
					<div class="row income hide-all-display">
						
						<div class="col-md-4">
							<?= $form->field($employments, 'monthly_income')->textInput(['type' => 'number']) ?>
						</div>
						<div class="col-md-4 employed">
							<?= $form->field($employments, 'occupation') ?>
						</div>
						
					</div>	
				</div>	
				
			
				<div class="basic-info-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-primary pull-right next-address" > Next </a>
						</div>
					</div>
                </div>
				<div class="address-info-btn hide-all-btn">
					<div class="row">
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-basic" > Previous </a>
							<a href="javascript:void(0)" class="btn btn-primary pull-right employment-next" > Next </a>
						</div>
					</div>
                </div>
				<div class="employment-info-btn hide-all-btn">				
					<div class="row">				
						<div class="col-md-12 form-group">
							<a href="javascript:void(0)" class="btn btn-default prev-address" > Previous </a>
							<?= Html::submitButton('Save', ['class' => 'btn btn-primary pull-right', 'name' => 'save']) ?>
						</div>
					</div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

