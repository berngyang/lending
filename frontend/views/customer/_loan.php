<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div id="apply-loan-form">
	
    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($model, 'amount')->textInput(['type' => 'number']) ?>
		</div>
		
	</div>
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($model, 'term')
								 ->dropDownList(
										array(
										""=>"Select Terms",
										3=>"3 Months",
										6=>"6 Months",
										9=>"9 Months",
										12=>"12 Months",
										)
									)
							?>
		</div>
		
	</div>
    <div class="row">
		
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($model, 'interest')->hiddenInput()->label(false) ?>
			<?= $form->field($model, 'interest_input')->textInput(['type' => 'number', 'value' => 0,'disabled' => true]) ?>
		</div>

	</div>
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($model, 'amortization')->hiddenInput()->label(false) ?>
			<?= $form->field($model, 'amortization_input')->textInput(['type' => 'number', 'value' => 0,'disabled' => true]) ?>
		</div>

	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<?= Html::submitButton('Submit', ['class' => 'btn btn-success pull-right']) ?>
			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
