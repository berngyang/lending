<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $customer->first_name." ".$customer->middle_name." ".$customer->last_name;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


?>
	



<div class="user-view">
	<div class="row padding-bottom-10">
		<div class="col-md-12">
			<?= Html::a('Update Account', ['update'], ['class' => 'btn btn-success pull-right']) ?>
		</div>
	</div>
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row panel panel-default">
					<div class="container-fluid">	
						<div class="row">
							<div class="col-md-12">
								<h3>Personal Details</h3>
							</div>
							<div class="col-md-6">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Basic Information</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>First Name</td>
											<td><?= $customer->first_name ?></td>
										</tr>
										<tr>
											<td>Middle Name</td>
											<td><?= $customer->middle_name ?></td>
										</tr>
										<tr>
											<td>Last Name</td>
											<td><?= $customer->last_name ?></td>
										</tr>
										<tr>
											<td>Age</td>
											<td><?= $customer->age ?></td>
										</tr>
										<tr>
											<td>Gender</td>
											<td><?= $customer->gender ?></td>
										</tr>
										<tr>
											<td>Birthday</td>
											<td><?= $customer->birthday ?></td>
										</tr>
										<tr>
											<td>Civil Status</td>
											<td><?= $customer->civilStatus ?></td>
										</tr>
									</tbody>
								</table>
								
								</br>
							</div>
							<div class="col-md-6">
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Address Details</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>House Number</td>
											<td><?= $customer->address->house_number ?></td>
										</tr>
										<tr>
											<td>Street</td>
											<td><?= $customer->address->street ?></td>
										</tr>
										<tr>
											<td>Barangay</td>
											<td><?= $customer->address->barangay ?></td>
										</tr>
										<tr>
											<td>City</td>
											<td><?= $customer->address->city ?></td>
										</tr>
										<tr>
											<td>Province</td>
											<td><?= $customer->address->province ?></td>
										</tr>
										<tr>
											<td>Postal Code</td>
											<td><?= $customer->address->post_code ?></td>
										</tr>
									</tbody>
								</table>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<?php 
										
										if($customer->employments->source_of_income != 4) { //Business Owner ?>
											<th colspan="3">Employment Details</th>
										<?php  } else { ?>
											<th colspan="1">Employment Details</th>
										<?php  } ?>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>Source Of Income</td>
											<td><?= $customer->address->house_number ?></td>
										</tr>
										<?php if($customer->employments->source_of_income != 4) { //Business Owner ?>
										<tr>
											<td>Monthly Income</td>
											<td><?= $customer->employments->monthly_income ?></td>
										</tr>
										<tr>
											<td>Occupation</td>
											<td><?= $customer->employments->occupation ?></td>
										</tr>
										<?php  } ?>
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		
			
				
		</div>	
	</div>	
	
	<div class="container">	
		<div class="row panel panel-default">
			<div class="container">	
				<div class="col-md-6">
					<h3>GCASH Account</h3>
				</div>
				<div class="col-md-6">
					<h3>
						 <?php if ($customer->gcashAccount == null) { ?>
							<h3>
								<a href="/gcash/add" class="btn btn-success pull-right">Add Gcash Account</a>
							</h3>
					<?php } else { ?>
							<h3>
								<a href="/gcash/update?id=<?=$customer->gcashAccount->id?>" class="btn btn-success pull-right">Update Gcash Account</a>
							</h3>
					<?php } ?>
					</h3>
				</div>
				<table class="table table-striped  table-bordered">
					<thead>
					  <tr>
						<td>Full Name</td>
						<td>Phone Number</td>
						<td>Email</td>
						<td>Status</td>
					  </tr>
					</thead>
					
					<tbody>
						<?php 
								if($customer->gcashAccount != null) { ?>
									
									<tr>
										<td><?= $customer->gcashAccount->full_name ?></td>
										<td><?= $customer->gcashAccount->phone_number ?></td>
										<td><?= $customer->gcashAccount->email ?></td>
										<td><?= $customer->gcashAccount->statusText ?></td>
									</tr>
						<?php 		
								} else { ?>
								<tr>
									<td colspan="4">No found record.</td>
								</tr>
						<?php }
						?>
						
					</tbody>
				</table>

				</br>
			</div>

		</div>

	</div>
	
	<div class="container">	
		<div class="row panel panel-default">
			<div class="container">	
				<h3>Documents</h3>
				<table class="table table-bordered">
					<thead>
						<th>File</th>
						<th>Action</th>
					</thead>
					<tbody>
						<?php 
							
							if($customer->documents != null && $customer->signature != null) {
								foreach($customer->documents as $key => $value)
								{
									 
									$keyString = "";
									if($key == "valid_id") {
										$keyString="Valid ID";
									} else if($key == "secondary_id") {
										$keyString="Secondary ID";
									} else if($key == "proof_of_billing") {
										$keyString="Proof of Billing";
									}
									
									if($keyString != "") {
										echo "<tr>";
										
										echo "<td>".$keyString."</td>";
										echo "<td><a href=".Yii::getAlias('@web').'/uploads/customer/documents/'.$value." target='_blank'> View Docs</a></td>";
										echo "</tr>";
									}
								}
								
								echo "<tr>";
								echo "<td>Signature</td>";
								echo "<td><a href=".Yii::getAlias('@web').'/uploads/customer/signature/'.$customer->signature." target='_blank'> View Docs</a></td>";
								echo "</tr>";
								
							} else {
								echo "<tr><td colspan='2'>No found record.</td></tr>";
							}
						?>
						
					</tbody>
				</table>

				</br>
			</div>

		</div>

	</div>
	
	<div class="container">	
		<div class="row panel panel-default">
			<div class="container">	
				<h3>Additional Documents</h3>
				<table class="table table-bordered">
					<thead>
						<th>File</th>
						<th>Action</th>
					</thead>
					<tbody>
						<?php 
							if($customer->loans != null) {
								foreach ($customer->loans as $loans) { 
									foreach ($loans->loanAttachments as $attachment) { ?>
										<tr>
											<td><?= $attachment->filename ?></td>
											<td><a href="<?= Yii::getAlias('@web').'/uploads/customer/attachments/'.$attachment->path ?>" target="_blank"> View Docs</a></td>
										</tr>
					<?php 			}
								}
							} else { ?>
							<tr>
								<td colspan="2">No found record.</td>
							</tr>
					<?php }
					?>
						
					</tbody>
				</table>

				</br>
			</div>

		</div>

	</div>
	
	
	
	
</div>
