<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;


		
?>
<div class="site-index">
	<div class="row padding-bottom-10">
		<div class="col-md-12">
			<?php 
				if(empty($applyLoan)) {
					echo Html::a('Apply Loan', ['apply-loan'], ['class' => 'btn btn-success pull-right']); 
				}
			

			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
					<div class="col-md-12">
						<h3>Pending Loan</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $pendingLoan,
								'columns' => [
									
									[
									  'attribute' => 'amount'
									],
									[
									  'attribute' => 'amortization'
									],
									[
									  'attribute' => 'created'
									],
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-pending-loan}',
										
										'buttons'=> [
											'view-pending-loan' => function ($url, $model) {     

												return Html::a('View', ['/loan/view', 'id' => $model->id],['target'=>'_blank']);                              

											}
											
										]
									]
								]
							]); ?>
							
							</br>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
				
					<div class="col-md-12">
						<h3>Active Loan</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $activeLoan,
								'columns' => [
									
									[
									  'attribute' => 'amount'
									],
									[
									  'attribute' => 'amortization'
									],
									[
									  'attribute' => 'created'
									],
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-active-loan}',
										
										'buttons'=> [
											'view-active-loan' => function ($url, $model) {     

												return Html::a('View', ['/loan/view', 'id' => $model->id],['target'=>'_blank']);                              

											}
											
										]
									]
								]
							]); ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
			
	</div>	
	
	<div class="row">
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
					<div class="col-md-12">
						<h3>Done Loan</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $doneLoan,
								'columns' => [
									
									[
									  'attribute' => 'amount'
									],
									[
									  'attribute' => 'amortization'
									],
									[
									  'attribute' => 'created'
									],
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-done-loan}',
										
										'buttons'=> [
											'view-done-loan' => function ($url, $model) {     

												return Html::a('View', ['/loan/view', 'id' => $model->id],['target'=>'_blank']);                              

											}
											
										]
									]
								]
							]); ?>
							
						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="container-fluid">	
				<div class="row panel panel-default">
					<div class="col-md-12">
						<h3>Payment Due</h3>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<?= GridView::widget([
								'dataProvider' => $overDueLoan,
								'columns' => [
									
									[
									  'attribute' => 'amount'
									],
									[
									  'attribute' => 'amortization'
									],
									[
									  'attribute' => 'created'
									],
									['class' => 
										'yii\grid\ActionColumn', 
										'header'=>'Actions',
										'template' => '{view-over-due-loan}',
										
										'buttons'=> [
											'view-over-due-loan' => function ($url, $model) {     

												return Html::a('View', ['/loan/view', 'id' => $model->id],['target'=>'_blank']);                              

											}
											
										]
									]
								]
							]); ?>
							
						
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
			
	</div>	
	
</div>
