<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Add Remittance Slip' ;
$this->params['breadcrumbs'][] = 'Add Remittance Slip';
?>
<div class="remittance-slip">


    <?= $this->render('_remittance', [
        'paymentModel' => $paymentModel,
        'model' => $model,
    ]) ?>

</div>
