<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div id="apply-loan-form">
	
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'action'=>'add-remittance']); ?>
	
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($paymentModel, 'loan_id')->hiddenInput(['value'=>$model->id])->label(false); ?>
			<?= $form->field($paymentModel, 'amount')->textInput(['type' => 'number']) ?>
		</div>
		
	</div>
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($paymentModel, 'imageFile')->fileInput() ?>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<?= Html::submitButton('Submit', ['class' => 'btn btn-success pull-right']) ?>
			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
