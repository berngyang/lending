<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\widgets\ActiveForm;


$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
	


<div class="loan-view" id="loan-view-page">
	
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row panel panel-default">
					<div class="container-fluid">	
						<div class="row">
							<div class="col-md-6">
								<h3>Loan 
									<span>
									<?php if($model->status == $model::STATUS_ACTIVE) { ?>
										Reference: 1234-5678-91011 (remitance)
									<?php } ?>
									</span>
								
								</h3>
								
							</div>
							<div class="col-md-6">
								<?php if($model->status == $model::STATUS_PENDING) { ?>
									<h3>
										<a href="javascript:void(0)" class="btn btn-danger pull-right" data-toggle ="modal" data-target="#cancel-loan-modal" >Cancel Loan</a>
									</h3>
								<?php }?>
							</div>
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<th colspan="2">Loan Details</th>
									  </tr>
									</thead>
									
									<tbody>
										<tr>
											<td>Amount</td>
											<td><?= $model->amount ?></td>
										</tr>
										<tr>
											<td>Status</td>
											<td><?= $model->statusLabel ?></td>
										</tr>
										<tr>
											<td>Amortization</td>
											<td><?= $model->amortization ?></td>
										</tr>
										<tr>
											<td>interest</td>
											<td><?= $model->interest ?></td>
										</tr>
										<tr>
											<td>Term</td>
											<td><?= $model->term ?></td>
										</tr>
										<?php if( $model->total_paid > 0) { ?>
											<tr>
												<td>Total Paid</td>
												<td><?= $model->total_paid ?></td>
											</tr>
										<?php } ?>
										<tr>
											<td>Total Due</td>
											<td><?= $model->total_due ?></td>
										</tr>
										
										<tr>
											<td>Due Date</td>
											<td><?= $model->due_date ?></td>
										</tr>
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<h3>Remarks</h3>
							</div>
							
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<td>Remarks</td>
										<td>Created</td>
									  </tr>
									</thead>
									
									<tbody>
										<?php 
												if($model->loanRemarks != null) {
													foreach ($model->loanRemarks as $remark) { ?>
														<tr>
															<td><?= $remark->remarks ?></td>
															<td><?= $remark->created ?></td>
														</tr>
										<?php 		}
												} else { ?>
												<tr>
													<td colspan="2">No found record.</td>
												</tr>
										<?php }
										?>
										
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<h3>Attachments</h3>
							</div>
							<div class="col-md-6">
								<?php if($model->status == $model::STATUS_PENDING) { ?>
								<h3>
									<a href="javascript:void(0)" class="btn btn-primary pull-right" data-toggle ="modal" data-target="#attachment-loan-modal" >Add Attachments</a>
								</h3>
								<?php } ?>
							</div>
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<td>File Name</td>
										<td>Action</td>
									  </tr>
									</thead>
									
									<tbody>
										<?php 
												if($model->loanAttachments != null) {
													foreach ($model->loanAttachments as $attachment) { ?>
														<tr>
															<td><?= $attachment->filename ?></td>
															<td><a href="<?= Yii::getAlias('@web').'/uploads/customer/attachments/'.$attachment->path ?>" target="_blank"> View Docs</a></td>
														</tr>
										<?php 		}
												} else { ?>
												<tr>
													<td colspan="2">No found record.</td>
												</tr>
										<?php }
										?>
										
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<h3>Payments</h3>
							</div>
							<div class="col-md-6">
								 <?php if ($model->status == $model::STATUS_ACTIVE) { ?>
									<h3>
										<a href="/loan/payment?id=<?=$model->id?>" class="btn btn-primary pull-right">Pay Via Gcash</a>
									</h3>
									<h3>
										<a href="/loan/remittance?id=<?=$model->id?>" class="btn btn-success pull-right">Add Remitance Slip</a>
									</h3>
							<?php } ?>
							</div>
							<div class="col-md-12">
								
								<table class="table table-striped  table-bordered">
									<thead>
									  <tr>
										<td>Amount</td>
										<td>Payment Status</td>
										<td>Payment Date</td>
										<td>Payment Type</td>
										<td>Remarks</td>
									  </tr>
									</thead>
									
									<tbody>
										<?php 
												if($model->payments != null) {
													foreach ($model->payments as $payment) { ?>
														<tr>
															<td><?= $payment->amount ?></td>
															<td><?= $payment->paymentStatusText ?></td>
															<td><?= $payment->payment_date ?></td>
															<td><?= $payment->paymentTypeText ?></td>
															<td><?= $payment->remarks ?></td>
														</tr>
										<?php 		}
												} else { ?>
												<tr>
													<td colspan="5">No found record.</td>
												</tr>
										<?php }
										?>
										
									</tbody>
								</table>
								
								</br>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>	
	</div>	
	
	<div class="modal fade" id="cancel-loan-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<form id="form-cancel-loan" >
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Cancellation of Loan</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible cancel-loan-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Your loan has been cancelled.
							</div>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="Loans[id]" id="loan_id" value="<?php echo $model->id;?>" />
						<input type="hidden" name="Loans[status]" value="<?php echo $model::STATUS_CANCELLED;?>" />
					
						<div class="col-md-10 col-md-offset-1">

							<div class="form-group">
								<label>Do you want to cancel your loan?</label>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default btn-success cancel-loan-submit">Yes</button>
				</div>
			</div>
	  
		</form>
		</div>
	</div>
	
	
	<div class="modal fade" id="attachment-loan-modal" role="dialog">
		<div class="modal-dialog">
	
		<!-- Modal content-->
		<?php $form = ActiveForm::begin(['action' => ['loan/add-attachments'],'options' => ['enctype' => 'multipart/form-data']]); ?>
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close close-button-object" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Add attachment</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success alert-dismissible attachment-loan-message">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> File has been uploaded.
							</div>
						</div>
					</div>
						
					<div class="row">
						<?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false) ?>
						<div class="col-md-12">
							<?= $form->field($model, 'filename') ?>
							
						</div>
						<div class="col-md-12">
							<?= $form->field($model, 'imageFile')->fileInput() ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-submit']) ?>
				</div>
			</div>
	  
		<?php ActiveForm::end(); ?>
		</div>
	</div>
	
</div>
