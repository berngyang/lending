<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\widgets\ActiveForm;


$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

 
?>
	


		
<div class="loan-view" id="loan-view-page">
	
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				Pay now via Gcash!
			</div>	
			<div class="col-md-12">
				<?=$button?>
			</div>
			<div class="col-md-12">
				<i>Note*: Payment may take a while to take effect. If you already paid this transaction please ignore payment process and wait for the payment confirmation.
					Thank you for your kind consideration.
				</i>
			</div>
		</div>	
	</div>	
	
	
</div>
