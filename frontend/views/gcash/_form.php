<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div id="apply-loan-form">
	
    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			
			<?= $form->field($model, 'full_name') ?>
		</div>
		
	</div>
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($model, 'phone_number')->textInput(['type' => 'number']) ?>
		</div>
		
	</div>
	<div class="row">
		
		<div class="col-md-6 col-md-offset-3">
			<?= $form->field($model, 'email') ?>
		</div>
		
	</div>
    
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<?= Html::submitButton('Submit', ['class' => 'btn btn-success pull-right']) ?>
			</div>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
