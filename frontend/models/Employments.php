<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employments".
 *
 *
 */
class Employments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['occupation'], 'string', 'max' => 255],
			['source_of_income', 'required'],
			['monthly_income', 'required'],
			['occupation', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_of_income' => 'Source of Income',
            'monthly_income' => 'Monthly Income',
            'occupation' => 'Occupation'
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['employment_id' => 'id']);
    }
}
