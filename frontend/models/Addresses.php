<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property int $id
 * @property string|null $street
 * @property string|null $city
 * @property string|null $province
 * @property string|null $post_code
 * @property int|null $status
 * @property string|null $updated
 * @property string|null $created
 *
 * @property User[] $users
 */
class Addresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['house_number','street','barangay', 'city', 'province', 'post_code'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'street' => 'Street',
            'house_number' => 'House Number',
            'barangay' => 'Barangay',
            'city' => 'City',
            'province' => 'Province',
            'post_code' => 'Post Code',
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['address_id' => 'id']);
    }
}
