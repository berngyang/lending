<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use common\models\Customers;
use common\models\Addresses;
use app\models\Employments;
use app\models\CustomerDocuments;
use yii\web\UploadedFile;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $address_id;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $birthday;
    public $gender;
    public $phone_number;
    public $confirm_password;
    public $street;
    public $barangay;
    public $house_number;
    public $signature;
    public $city;
    public $province;
    public $post_code;
    public $source_of_income;
    public $monthly_income;
    public $occupation;
    public $valid_id;
    public $civil_status;
    public $age;
    public $secondary_id;
    public $proof_of_billing;
    public $document_id;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		
	
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\Customers', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 5, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Customers', 'message' => 'This email address has already been taken.'],
		
			['phone_number', 'string', 'min' => 11,'max' => 11],
			
			[['source_of_income', 'monthly_income','occupation','valid_id','secondary_id','proof_of_billing','signature'], 'safe'],
            ['first_name', 'required'],
            ['middle_name', 'required'],
            ['last_name', 'required'],
            ['birthday', 'required'],
            ['gender', 'required'],
            ['house_number', 'required'],
            ['barangay', 'required'],
            ['street', 'required'],
            ['city', 'required'],
            ['province', 'required'],
            ['post_code', 'required'],
            ['age', 'required'],
            ['civil_status', 'required'],
			
			
			
            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
			
			['confirm_password', 'required'],
			['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ]
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
		try{
			if (!$this->validate()) {
				return null;
			}
			
			$customer = new Customers();
			$address = new Addresses();
			$employment = new Employments();
			$customerDocuments = new CustomerDocuments();
			$uploadValidID = new CustomerDocuments();
			$uploadSecondaryID = new CustomerDocuments();
			$uploadProofOfBilling = new CustomerDocuments();
			
			$uploadValidID->imageFile = UploadedFile::getInstance($this, 'valid_id');
			$uploadSecondaryID->imageFile = UploadedFile::getInstance($this, 'secondary_id');
			$uploadProofOfBilling->imageFile = UploadedFile::getInstance($this, 'proof_of_billing');
			
			if($uploadValidID->imageFile != null && $uploadSecondaryID->imageFile != null && $uploadProofOfBilling->imageFile != null) {				
				
				if(!file_exists(Yii::getAlias('@backend').'/web/uploads/customer/documents/')) {
					mkdir(Yii::getAlias('@backend').'/web/uploads/customer/documents/',0777,true);
				}
				if(!file_exists(Yii::getAlias('@frontend').'/web/uploads/customer/documents/')) {
					mkdir(Yii::getAlias('@frontend').'/web/uploads/customer/documents/',0777,true);
				}
				
				if(!file_exists(Yii::getAlias('@backend').'/web/uploads/customer/signature/')) {
					mkdir(Yii::getAlias('@backend').'/web/uploads/customer/signature/',0777,true);
				}
				if(!file_exists(Yii::getAlias('@frontend').'/web/uploads/customer/signature/')) {
					mkdir(Yii::getAlias('@frontend').'/web/uploads/customer/signature/',0777,true);
				}
				
				$validIDPath = $uploadValidID->upload();
				$secondaryIDPath = $uploadSecondaryID->upload();
				$proofOfBillingPath = $uploadProofOfBilling->upload();
				
				$customerDocuments->valid_id = $validIDPath;
				$customerDocuments->secondary_id = $secondaryIDPath;
				$customerDocuments->proof_of_billing = $proofOfBillingPath;
				$customerDocuments->save(false);
				$this->document_id = $customerDocuments->id;
				
				
			}
				
			$customer->username = $this->username;
			$customer->email = $this->email;
			$customer->first_name = $this->first_name;
			$customer->middle_name = $this->middle_name;
			$customer->last_name = $this->last_name;
			$customer->phone_number = $this->phone_number;
			$customer->gender = $this->gender;
			$customer->civil_status = $this->civil_status;
			$customer->age = $this->age;
			$customer->birthday = $this->birthday;
			$customer->signature = $this->signature;
			$customer->document_id = $this->document_id;
			
			$customer->setPassword($this->password);
			$customer->generateAuthKey();
			$customer->generateEmailVerificationToken();
			
			//Address
			$address->street = $this->street;
			$address->house_number = $this->house_number;
			$address->barangay = $this->barangay;
			$address->city = $this->city;
			$address->province = $this->province;
			$address->post_code = $this->post_code;
			
			$address->save(false);
			$customer->address_id = $address->id;
			
			//Employment
			$employment->source_of_income = $this->source_of_income;
			$employment->monthly_income = $this->monthly_income;
			$employment->occupation = $this->occupation;
			
			$employment->save();
			$customer->employment_id = $employment->id;
			
			return $customer->save() && $this->sendEmail($customer);
		} catch(Exception $err) {
			var_dump($err);exit;
		}

    }
	
    protected function sendEmail($customer)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['customer' => $customer]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
	public function getCustomerDocumentsModel()
	{
		return new CustomerDocuments();
	}
	
}
