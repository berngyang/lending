<?php

namespace app\models;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "file_uploads".
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $created
 * @property string|null $updated
 */
class UploadForm extends Model
{
	
	public $imageFile;
    /**
     * {@inheritdoc}
     */
	
    public static function tableName()
    {
        return 'file_uploads';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, pdf']
        ];
    }
    
    public function uploadAttachments()
    {
		
        if ($this->validate()) {
			$filePath = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::getAlias('@backend').'/web/uploads/customer/attachments/' .$filePath );
			$copied = copy(Yii::getAlias('@backend').'/web/uploads/customer/attachments/' .$filePath  ,Yii::getAlias('@frontend').'/web/uploads/customer/attachments/' .$filePath );
			return $filePath;
        } else {
            return false;
        }
    }
	
	public function upload()
    {
        if ($this->validate()) {
			$filePath = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::getAlias('@backend').'/web/uploads/customer/documents/' .$filePath );
			
			return $filePath;
        } else {
            return false;
        }
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Upload File',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }
}
