<?php

namespace app\models;

use Yii;
use yii\base\Model;
use common\models\Customers;
use app\models\Loans;
use yii\web\UploadedFile;

class Attachments extends \yii\db\ActiveRecord
{
   
	public $imageFile;
    public static function tableName()
    {
        return 'attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'filename' => 'File name',
            'loan_id' => 'Loan',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }
	public function getLoan()
    {
        return $this->hasOne(Loans::className(), ['id' => 'loan_id']);
    }
	
	public function uploadAttachments()
    {
        if ($this->validate()) {
			$filePath = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::getAlias('@frontend').'/web/uploads/customer/attachments/' .$filePath );
			$copied = copy(Yii::getAlias('@frontend').'/web/uploads/customer/attachments/' .$filePath  ,Yii::getAlias('@backend').'/web/uploads/customer/attachments/' .$filePath );
			return $filePath;
        } else {
            return false;
        }
    }
	
    public function uploadAttachmentsFrontEnd()
    {
		$filePath = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
		$this->imageFile->saveAs(Yii::getAlias('@frontend').'/web/uploads/customer/attachments/' .$filePath );
		return $filePath;
        
    }
	
	public function upload()
    {
        if ($this->validate()) {
			$filePath = $this->imageFile->baseName."_" .date("Y-m-d-h-i-s"). '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::getAlias('@backend').'/web/uploads/customer/documents/' .$filePath );
			
			return $filePath;
        } else {
            return false;
        }
    }
}
