<?php

namespace app\models;

use Yii;
class Remarks extends \yii\db\ActiveRecord
{
   
    public static function tableName()
    {
        return 'remarks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created','status'], 'safe'],
            ['remarks', 'required'],
            ['loan_id', 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'remarks' => 'Remarks',
            'loan_id' => 'Loan',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }
	
	public function getCustomer()
    {
        return $this->hasOne(Loans::className(), ['id' => 'loan_id']);
    }
	
	
}
