<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use common\models\Payments;
class Loans extends \yii\db\ActiveRecord
{
	const STATUS_PENDING = 0;
	const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
	const STATUS_DELETED = 3;
	const STATUS_DONE = 4;
	const STATUS_OVER_DUE = 5;
	const STATUS_CANCELLED = 6;
	const STATUS_DECLINED = 7;
	
	
	const TERM_1 = 1;
	const TERM_3 = 3;
	const TERM_6 = 6;
	const TERM_9 = 9;
	const TERM_12 = 12;
    
	public $imageFile;
	public $filename;
	
    public static function tableName()
    {
        return 'loans';
    }

    public function rules()
    {
        return [
			[['id', 'status','customer_id','is_due','amount'], 'integer'],
			[['created','status','filename'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['amount', 'required'],
            ['amortization', 'required'],
            ['interest', 'required'],
            ['term', 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Amount',
            'amortization' => 'Amortization',
            'interest' => 'Interest',
            'total_due' => 'Total Due',
            'total_paid' => 'Total Paid',
            'term' => 'Term',
            'due_date' => 'Due Date',
        ];
    }

	public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }
	
	public function getLoanRemarks()
    {
        return $this->hasMany(Remarks::className(), ['loan_id' => 'id']);
    }
	public function getActivePayments()
    {
        return $this->hasOne(Payments::className(), ['loan_id' => 'id'])->andOnCondition(['status' => self::STATUS_ACTIVE]);
    }
	public function getpayments()
    {
        return $this->hasMany(Payments::className(), ['loan_id' => 'id'])->andOnCondition(['<>','status', self::STATUS_DELETED]);
    }
	public function getLoanAttachments()
    {
        return $this->hasMany(Attachments::className(), ['loan_id' => 'id']);
    }
	
	
	public function getAllPendingLoans()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_PENDING,
			'customer_id' => Yii::$app->user->identity->id
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	
	public function getAllActiveLoans()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_ACTIVE,
			'customer_id' => Yii::$app->user->identity->id
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	public function getAllDoneLoans()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_DONE,
			'customer_id' => Yii::$app->user->identity->id
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	
	public function getAllOverDueLoans()
	{
		$query = self::find();
		$query->andFilterWhere([
            'status' => self::STATUS_OVER_DUE,
			'customer_id' => Yii::$app->user->identity->id
        ]);
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		return $dataProvider;
	}
	
	public function getCheckCurrentLoanStatus()
	{
		
		return self::find() 
		->andWhere(['customer_id'=>Yii::$app->user->identity->id])
		->andWhere(['or',['status'=>self::STATUS_ACTIVE],['status'=>self::STATUS_PENDING],['status'=>self::STATUS_OVER_DUE]])
		->all();
	}
	
	public function applyLoan()
	{
		
		$totalInterestAmount = $this->amount * $this->interest;
		$rate = 0.1;
		
		$this->customer_id = Yii::$app->user->identity->id;
		$this->total_due = $this->amount + $totalInterestAmount;
		if (!$this->validate()) {
            return null;
        }
		
		
		
		return $this->save();
	}
	
	public function getStatusLabel()
	{
		$statusLabel = [
			self::STATUS_PENDING => '<label class="badge badge-warning">Pending</label>',
			self::STATUS_ACTIVE => '<label class="badge badge-primary">Active</label>',
			self::STATUS_INACTIVE => '<label class="badge badge-danger">Inactive</label>',
			self::STATUS_DELETED => '<label class="badge badge-danger">Deleted</label>',
			self::STATUS_DONE => '<label class="badge badge-success">Done</label>',
			self::STATUS_OVER_DUE => '<label class="badge badge-danger">Over Due</label>',
			self::STATUS_CANCELLED => '<label class="badge badge-danger">Cancelled</label>',
			self::STATUS_DECLINED => '<label class="badge badge-danger">Declined</label>',
		];
		return $statusLabel[$this->status];

	}
	
	public function addAttachments()
    {
		
        $attachments = new Attachments();
		
		$attachments->imageFile = UploadedFile::getInstance($this, 'imageFile');
		
		if($attachments->imageFile != null) {				
			
			if(!file_exists(Yii::getAlias('@backend').'/web/uploads/customer/attachments/')) {
				mkdir(Yii::getAlias('@backend').'/web/uploads/customer/attachments/',0777,true);
			}
			if(!file_exists(Yii::getAlias('@frontend').'/web/uploads/customer/attachments/')) {
				mkdir(Yii::getAlias('@frontend').'/web/uploads/customer/attachments/',0777,true);
			}
			
			$path = $attachments->uploadAttachments();
			
			// $backend = $uploadForm->uploadAttachments();
			$attachments->filename = $this->filename;
			$attachments->path = $path;
			$attachments->loan_id = $this->id;
			$attachments->customer_id = Yii::$app->user->identity->id;
			return $attachments->save(false);
			
		}
		
		return false;
    }
}
