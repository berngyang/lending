<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php',
	
);

return [
    'id' => 'app-frontend',
	'name' => 'Lending',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
			// 'class' => 'common\components\Request',
			// 'web'=> '/frontend/web'
        ],
		'user' => [
            'identityClass' => 'common\models\Customers',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
      
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'lending505@gmail.com',
                'password' => 'lendingloan2021',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],


    ],
    'as access' => [
        'class' => yii\filters\AccessControl::class,
        'except' => ['site/error','site/request-password-reset','site/reset-password','site/upload-signature', 'site/login', 'site/logout','site/signup','site/index','site/verify-email'],
        'rules' => [
            ['allow' => true, 'roles' => ['@']],
        ],
    ],
    'params' => $params,
];
