

	

$( document ).ready(function() {
	if ($('#loan-view-page').length > 0) {
		$("body").on("click",".cancel-loan-submit",function(){
			
			var formData = $('#form-cancel-loan');
			var id = $('#loan_id').val();
		
			$.ajax({

				url :'/loan/cancel?id='+id,
				type : 'POST',
				data : formData.serialize(),
				dataType:'json',
				success : function(data) {   
					if(data.response) {
						
						$('.cancel-loan-message').css("display","block");
						
						setTimeout(function(){ 
							 location.reload();
						}, 1500);
					} 
				},
				error : function(request,error)
				{
					alert("Request: "+JSON.stringify(error));
				}
			});
			
		});
		
		
		$("body").on("click",".attachment-loan-submit",function(){
			
			var formData = $('#form-attachment-loan');
			var id = $('#loan_id').val();
		
			$.ajax({

				url :'/loan/add-attachments?id='+id,
				type : 'POST',
				data : formData.serialize(),
				dataType:'json',
				success : function(data) {   
					if(data.response) {
						
						$('.attachment-loan-message').css("display","block");
						
						setTimeout(function(){ 
							 location.reload();
						}, 1500);
					} 
				},
				error : function(request,error)
				{
					alert("Request: "+JSON.stringify(error));
				}
			});
			
		});
		
	}
});


