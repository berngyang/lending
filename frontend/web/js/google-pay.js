
const cardPaymentMethod = {
	type:"CARD",
	tokenizationSpecification:tokenizationSpecification,
	parameters:{
		allowedCardNetworks:["VISA","MASTERCARD"],
		allowedAuthMethods:["PAN_ONLY","CRYPTOGRAM_3DS"]
	}
}

const googlePayConfiguration = {
	apiVersion:2,
	apiVersionMinor:0,
//	allowedPaymentMethods:[cardPaymentMethod],
}
let googlePayClient;
function onGooglePayloaded(){
	googlePayClient = new google.payments.api.PaymentsClient({
		environment:"Test"
	});
	googlePayClient.isReadyToPay()
}