

	

$( document ).ready(function() {
	if ($('#sign-up-customer-page').length > 0) {
		$("body").on("click","#signupform-source_of_income",function(){
			
			
			var sourceOfIncome = $('#signupform-source_of_income').val();
			
			if(sourceOfIncome == 1) { //Self Employed
				$('.employed').css("display","block");
				$('.income').css("display","block");
			} else if (sourceOfIncome == 2) { //Private Employee
				$('.employed').css("display","block");
				$('.income').css("display","block");
			} else if (sourceOfIncome == 3) { //Government Employee
				$('.employed').css("display","block");
				$('.income').css("display","block");
			} else if (sourceOfIncome == 4) { //Business Owner
				$('.employed').css("display","none");
				$('.income').css("display","block");
			} else {
				$('.income').css("display","none");
				$('.employed').css("display","none");
			}
			
			
		});
		
		function hideAllButtons(){
			$('.hide-all-btn').css("display","none");
			$('.hide-all-form').css("display","none");
		}
		hideAllButtons();
		
		
		$("body").on("click",".next-address",function(){
			
			var ctrError = 0;
			$(".basic-info input").each(function() {
				if($(this).val() == "") {
					ctrError++;
					$('.field-'+$(this).attr('id')).addClass("has-error");
				} else {
					$('.field-'+$(this).attr('id')).removeClass("has-error");
				}
			});
			if($('#signupform-gender').val() == "") {
				$('.field-signupform-gender').addClass("has-error");
				ctrError++;
			}
			if(ctrError == 0){
				
				$('.basic-info').css("display","none");
				$('.address-info').fadeIn();
				$('.basic-info-btn').css("display","none");
				$('.address-info-btn').css("display","block");
			}
		});
		
		$("body").on("click",".prev-basic",function(){
			$('.basic-info').fadeIn();
			$('.address-info').css("display","none");
			$('.basic-info-btn').css("display","block");
			$('.address-info-btn').css("display","none");
		});
		
		
		$("body").on("click",".employment-next",function(){
			var ctrError = 0;
			$(".address-info input").each(function() {
				if($(this).val() == "") {
					ctrError++;
					$('.field-'+$(this).attr('id')).addClass("has-error");
				} else {
					$('.field-'+$(this).attr('id')).removeClass("has-error");
				}
			});
			if(ctrError == 0){
				$('.address-info').css("display","none");
				$('.employment-info').fadeIn();
				$('.address-info-btn').css("display","none");
				$('.employment-info-btn').css("display","block");
			}
		});
		
		$("body").on("click",".prev-address",function(){
			
			$('.address-info').fadeIn();
			$('.employment-info').css("display","none");
			$('.address-info-btn').css("display","block");
			$('.employment-info-btn').css("display","none");
		});
		
		$("body").on("click",".next-documents",function(){
			
			var ctrError = 0;
			var sourceOfIncome = $('#signupform-source_of_income').val();
			
			if( sourceOfIncome== "") {
				$('.field-signupform-source_of_income').addClass("has-error");
				ctrError++;
			} else {
				$('.field-signupform-source_of_income').removeClass("has-error");
				var monthlyIncome = $('#signupform-monthly_income').val();
				var occupation = $('#signupform-occupation').val();
				if(monthlyIncome == "") {
					$('.field-signupform-monthly_income').addClass("has-error");
					ctrError++;
				} else {
					$('.field-signupform-monthly_income').removeClass("has-error");
				}
				if(sourceOfIncome != 4 && sourceOfIncome != "") { //Business Owner
					if(occupation == "") {
						$('.field-signupform-occupation').addClass("has-error");
						ctrError++;
					} else {
						$('.field-signupform-occupation').removeClass("has-error");
					}
				}
			}
			
			if(ctrError == 0){
				$('.employment-info').css("display","none");
				$('.documents-info').fadeIn();
				$('.employment-info-btn').css("display","none");
				$('.documents-info-btn').css("display","block");
			}
		});
		$("body").on("click",".prev-employment",function(){
			
			$('.employment-info').fadeIn();
			$('.documents-info').css("display","none");
			$('.employment-info-btn').css("display","block");
			$('.documents-info-btn').css("display","none");
		});
		
		$("body").on("click",".next-terms-and-conditions",function(){
			
			var ctrError = 0;
			
			if(ctrError == 0){
				$('.documents-info').css("display","none");
				$('.terms-and-conditions-info').fadeIn();
				$('.documents-info-btn').css("display","none");
				$('.terms-and-conditions-info-btn').css("display","block");
			}
		});
		
		$("body").on("click",".prev-documents",function(){
			
			$('.documents-info').fadeIn();
			$('.terms-and-conditions-info').css("display","none");
			$('.documents-info-btn').css("display","block");
			$('.terms-and-conditions-info-btn').css("display","none");
		});
		
		$("body").on("click",".next-credential",function(){
			var ctrError = 0;
			if ($('#signArea').signaturePad().validateForm() == false) {
				ctrError++;
			}
			if(ctrError == 0){
				html2canvas([document.getElementById('sign-pad')], {
					
					onrendered: function (canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						//ajax call to save image inside folder
						$.ajax({
							url: '/site/upload-signature',
							data: { img_data:img_data },
							type: 'post',
							dataType: 'json',
							success: function (response) {
							   console.log(response.file_name);
							   $('#signupform-signature').val(response.file_name);
							}
						});
					}
				});
		
				$('.terms-and-conditions-info').css("display","none");
				$('.credential-info').fadeIn();
				$('.terms-and-conditions-info-btn').css("display","none");
				$('.credential-info-btn').css("display","block");
			}
		});
		
		$("body").on("click",".prev-terms-and-conditions",function(){
			
			$('.terms-and-conditions-info').fadeIn();
			$('.credential-info').css("display","none");
			$('.terms-and-conditions-info-btn').css("display","block");
			$('.credential-info-btn').css("display","none");
		});
		$("body").on("click",".clear-pad",function(){
		  $('#signArea').signaturePad().clearCanvas();
		});
		$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
		
		$("body").on("change","#signupform-source_of_income",function(){
			var sourceOfIncome = $(this).val();
			
			if(sourceOfIncome == 4) { //Business
				$('.income.hide-all-display').css("display","none");
				$('#signupform-monthly_income').val(null);
				$('#signupform-occupation').val(null);
			} else {
				
			}
		
		});
	}
});


