$(document).ready(function(){
	
	
	if($('#apply-loan-form').length > 0) {
		var rate = 0.1;
		var amortization =0;
		var interest =0;
		
		$("body").on("change","#loans-amount",function(){
			var amount = $(this).val();
			var term = $('#loans-term').val();
			
			if(amount == undefined || $.isNumeric(amount) == false ) {
				amount = 0;
			}
			if(term == undefined || $.isNumeric(term) == false ) {
				term = 0;
			}
			
			interest = rate * term;
			interest = interest.toFixed(2);
			var totalInterest =  interest * amount;
			
			var totalAmount = parseInt(totalInterest) + parseInt(amount);
			console.log(totalAmount);
			console.log(term);
			amortization = totalAmount/term;
			amortization =amortization.toFixed(2)
			$('#loans-interest').val(interest);
			$('#loans-interest_input').val(interest);
			$('#loans-amortization').val(amortization);
			$('#loans-amortization_input').val(amortization);
		});
		
		$("body").on("change","#loans-term",function(){
			
			var amount = $('#loans-amount').val();
			var term = $(this).val();
			
			if(amount == undefined || $.isNumeric(amount) == false ) {
				amount = 0;
			}
			if(term == undefined || $.isNumeric(term) == false ) {
				term = 0;
			}
			
			interest = rate * term;
			interest = interest.toFixed(2);
			var totalInterest =  interest * amount;
			
			var totalAmount = parseInt(totalInterest) + parseInt(amount);
			console.log(totalAmount);
			console.log(term);
			amortization = totalAmount/term;
			amortization =amortization.toFixed(2)
			$('#loans-interest').val(interest);
			$('#loans-interest_input').val(interest);
			$('#loans-amortization').val(amortization);
			$('#loans-amortization_input').val(amortization);
			
		});
		
	}

});