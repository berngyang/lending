<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Customers;
use common\models\GcashAccounts;
use yii\web\UploadedFile;
use conquer\select2\Select2Action;
date_default_timezone_set("Asia/Hong_Kong");
/**
 * Site controller
 */
class GcashController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	

	
    public function actionAdd()
    {
		$model = new GcashAccounts;
		$postData = Yii::$app->request->post();
		if(isset($postData['GcashAccounts'])) {
			$postData['GcashAccounts']['customer_id'] =Yii::$app->user->identity->id;
		}
		if ($model->load($postData) && $model->save()) {

            Yii::$app->session->setFlash('success', "Gcash has been created.!");
			return $this->redirect(['customer/my-account']);
        }

		return $this->render('add',[
			'model' => $model
		]);
		
	
    }
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success',  "Gcash has been updated.!");
            return $this->redirect(['customer/my-account']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
	protected function findModel($id)
    {
        if (($model = GcashAccounts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	
}
