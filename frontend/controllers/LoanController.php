<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Addresses;
use app\models\Loans;
use app\models\Employments;
use common\models\Payments;
use common\models\Customers;
use yii\web\UploadedFile;
use conquer\select2\Select2Action;

use \DateTime;
date_default_timezone_set("Asia/Hong_Kong");

class LoanController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	

	
	public function actionView($id)
    {
		$model =  $this->findModel($id);
		return $this->render('view',[
				'model' => $model
			]);
    }
	public function actionPayment($id)
    {
		$model =  $this->findModel($id);
		$payments = $model->activePayments;
		$newPayment = false;
		if( $payments != null) {
			$date1 = new DateTime($payments->created_date);
			$date2 = new DateTime();

			$diff = $date2->diff($date1);
			
			$hours = $diff->h;
			if($hours >= 6) {
				$payments->removePayment();
				$newPayment = true;
			}
		} else {
			$newPayment =true;
		}
		if($model->status == $model::STATUS_ACTIVE && $newPayment) {
			$paymentModel = new Payments;
			$payments = $paymentModel->createPaymentRequest($model);
			

		}
		
	
		$button = $payments->getButton();
		
		
		return $this->render('payment',[
				'model' => $model,
				'button' => $button,
			]);
    }
	
	public function actionRemittance($id)
	{
		$model =  $this->findModel($id);
		$paymentModel = new Payments;
		
		
		return $this->render('remittance',[
				'paymentModel' => $paymentModel,
				'model' => $model,
			]);
		
	}
	public function actionAddRemittance()
	{
		$postData = Yii::$app->request->post();
		$id = $postData['Payments']['loan_id'];
		$model =  $this->findModel($id);
		$paymentModel = new Payments;
		
		if ($paymentModel->load($postData) && $paymentModel->addRemittanceSlip()) {
			Yii::$app->session->setFlash('success', 'Remittance has been added.');
            return $this->redirect(['loan/view', 'id' => $id]);
		}
		foreach($paymentModel->errors as $error){
			Yii::$app->session->setFlash('error',$error);
		}
		
		
		return $this->redirect(['loan/remittance', 'id' => $id]);
		
	}
	
	public function actionAddAttachments()
    {
		$postData = Yii::$app->request->post();
		$id = $postData['Loans']['id'];
		$model =  $this->findModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->addAttachments()) {
			Yii::$app->session->setFlash('success', 'File has been uploaded.');
            
		}
		return $this->redirect(['loan/view', 'id' => $id]);
		
    }
	
	public function actionCancel($id)
    {

        $model = $this->findModel($id);
	
	
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			echo json_encode([
				'response'=>true,
				'message'=>"Success"
				
			]);
			exit;
			
        } else {
			
			 echo json_encode([
				'response'=>false,
				'message'=>"Failed"
				
			]);
			exit;
		}

       
    }
	
	protected function findModel($id)
    {
        if (($model = Loans::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	
}
