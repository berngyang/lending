<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Addresses;
use app\models\Loans;
use app\models\Employments;
use common\models\Customers;
use yii\web\UploadedFile;
use conquer\select2\Select2Action;
date_default_timezone_set("Asia/Hong_Kong");
/**
 * Site controller
 */
class CustomerController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	

	
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionMyAccount()
    {
		$customer = $this->findModel(Yii::$app->user->identity->id);
		return $this->render('my-account',[
					'customer' => $customer
				]);
		
	
    }
	
	public function actionLoan()
    {
		$loans = new Loans();
		$applyLoan = $loans->getCheckCurrentLoanStatus();
		$pendingLoan = $loans->getAllPendingLoans();
		$activeLoan = $loans->getAllActiveLoans();
		$doneLoan = $loans->getAllDoneLoans();
		$overDueLoan = $loans->getAllOverDueLoans();
		return $this->render('loan',[
					'pendingLoan' => $pendingLoan,
					'activeLoan' => $activeLoan,
					'doneLoan' => $doneLoan,
					'applyLoan' => $applyLoan,
					'overDueLoan' => $overDueLoan
				]);
		
	
    }
	public function actionApplyLoan()
    {
		$model = new Loans();
		if ($model->load(Yii::$app->request->post()) && $model->applyLoan()) {
            Yii::$app->session->setFlash('success', 'Thank you for applying for loan. Please check your loan status regularly.');
            return $this->redirect(['loan']);
        }
		
		return $this->render('apply-loan',[
					'model' => $model,
				]);
		
	
    }
	public function actionUploadSignature()
    {
		 $result = array();
		 $imagedata = base64_decode($_POST['img_data']);
		 $filename = md5(date("dmYhisA"));
		 //Location to where you want to created sign image
		 $file_name = './doc_signs/'.$filename.'.png';
		 file_put_contents($file_name,$imagedata);
		 $result['status'] = 1;
		 $result['file_name'] = $filename.'.png';
		 echo json_encode($result);
		
	
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CustomerLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current customers.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
		
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	public function actionUpdate()
    {
		$id = Yii::$app->user->identity->id;
        $model = $this->findModel($id);
		$addresses = $model->getUserAddress();
		$employments = $model->getUserEmployment();

		
        if ($model->load(Yii::$app->request->post()) && $model->customerUpdate($addresses,$employments )) {
			Yii::$app->session->setFlash('success',  "Account has been updated!");
            return $this->redirect(['customer/my-account', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'addresses' => $addresses,
            'employments' => $employments,
        ]);
    }
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
	
	
	protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
	
	
}
