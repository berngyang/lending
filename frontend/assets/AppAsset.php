<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery.signaturepad.css',
        'css/site.css',
    ];
    public $js = [
		'js/jquery.signaturepad.js',
		'js/html2canvas.js',
		'js/json2.min.js',
		'js/apply-loan.js',
		'js/loan-functions.js',
		'js/custom-update-customer.js',
		'js/custom-signup.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
	
}
